from os.path import dirname, abspath
import logging
import logging.handlers
from colorlog import ColoredFormatter

GLOBAL_PATH_APP = abspath(dirname(__file__))

ROOT_LOGGER = logging.getLogger('')
ROOT_LOGGER.setLevel(logging.DEBUG)

logging.getLogger('werkzeug').setLevel(logging.INFO)
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("peewee").setLevel(logging.INFO)
logging.getLogger("zeep").setLevel(logging.INFO)
logging.getLogger("googleads").setLevel(logging.INFO)
logging.getLogger("googleads.soap").setLevel(logging.WARNING)
logging.getLogger("google.auth.transport").setLevel(logging.WARNING)
logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)

__color_format = "%(log_color)s%(levelname)s%(reset)s | %(asctime)s |  %(fg_blue)s %(name)s %(reset)s >>>  %(message)s"
__text_format = "%(levelname)s | %(asctime)s |  %(name)s >>>  %(message)s"

formatter = logging.Formatter(__text_format, datefmt='%Y-%m-%d %H:%M')
formatter_color = ColoredFormatter(
    __color_format,
    datefmt='%Y-%m-%d %H:%M',
    reset=True,
    log_colors={
        'DEBUG': 'cyan',
        'INFO': 'green',
        'WARNING': 'yellow',
        'ERROR': 'red',
        'CRITICAL': 'red,bg_white',
    },
    secondary_log_colors={},
    style='%')

steam_handler = logging.StreamHandler()
steam_handler.setLevel(logging.DEBUG)
steam_handler.setFormatter(formatter_color)

handler_file = logging.handlers.RotatingFileHandler(
    f'{GLOBAL_PATH_APP}/log_application.log', maxBytes=(1048576 * 5), backupCount=7
)

handler_file.setLevel(logging.WARNING)
handler_file.setFormatter(formatter)

ROOT_LOGGER.addHandler(steam_handler)
ROOT_LOGGER.addHandler(handler_file)

from os import getenv, environ
from dotenv import load_dotenv
from logging_config import ROOT_LOGGER
from enum import Enum

load_dotenv()


class __AppGroupConfig(Enum):
    """ Родительский класс для настроек приложения.
    Проверяет, что в группах настроек переданны заданны все значения"""

    def __init__(self, env_name, default_value=None):
        self.__env = getenv(env_name)
        if default_value is not None and self.__env is None:
            self.__env = default_value

        if self.__env is None:
            raise AssertionError(f'В параметры окружения не была передана обязательная переменная '
                                 f'{env_name}\n'
                                 f'прочитайте README.md\n'
                                 f'Проверьте настройки приложения в разделе {self.__class__.__name__}')

    @property
    def val(self):
        """ Через этот атрибут возвращается значение из переменной окружения"""
        return self.__env


class DataBaseConfig(__AppGroupConfig):
    PORT = 'DATABASE_PORT'
    USER = 'DATABASE_USER'
    HOST = 'DATABASE_HOST'
    PASSWORD = 'DATABASE_PASSWORD'
    DATABASE = 'DATABASE_NAME'


class GoogleOAuthKey(__AppGroupConfig):
    """ Ключи приложения Google OAuth """
    CLIENT_ID = 'GOOGLE_OAUTH_CLIENT_ID'
    CLIENT_SECRET = 'GOOGLE_OAUTH_CLIENT_SECRET'
    GOOGLE_DISCOVERY_URL = 'GOOGLE_OAUTH_DISCOVERY_URL', "https://accounts.google.com/.well-known/openid-configuration"
    ACCESS_TOKEN_URI = 'ACCESS_TOKEN_URI', 'https://www.googleapis.com/oauth2/v4/token'
    AUTHORIZATION_URL = 'AUTHORIZATION_URL', 'https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&prompt=consent'
    AUTHORIZATION_SCOPE = 'AUTHORIZATION_SCOPE', 'openid email profile'
    AUTH_REDIRECT_URI = 'AUTH_REDIRECT_URI', 'http://127.0.0.1:5000/google/auth'
    BASE_URI = 'BASE_URI', 'http://127.0.0.1:5000'
    AUTH_TOKEN_KEY = 'AUTH_TOKEN_KEY', 'auth_token'
    AUTH_STATE_KEY = 'AUTH_STATE_KEY', 'auth_state'


class YandexDirectApiKey(__AppGroupConfig):
    """ Ключи приложения Direct """
    CLIENT_ID = 'YANDEX_CLIENT_ID'
    CLIENT_SECRET = 'YANDEX_CLIENT_SECRET'
    CALLBACK_URL = 'YANDEX_CALLBACK_URL', 'http://127.0.0.1:5000/direct/oauth'


class GoogleAdsApiKeys(__AppGroupConfig):
    """ Ключи приложения GOODLEAdS """
    CLIENT_ID = 'GOOGLE_CLIENT_ID'
    CLIENT_SECRET = 'GOOGLE_CLIENT_SECRET'
    PROJECT_ID = 'GOOGLE_PROJECT_ID'
    REDIRECT_URIS = 'GOOGLE_REDIRECT_URI', "http://127.0.0.1:5000/api/adwords/oauth2callback"
    DEVELOPER_TOKEN = 'GOOGLE_DEVELOPER_TOKEN'

    @staticmethod
    def get_web():
        return {
            'web': {
                "client_id": GoogleAdsApiKeys.CLIENT_ID.val,
                "client_secret": GoogleAdsApiKeys.CLIENT_SECRET.val,
                "project_id": GoogleAdsApiKeys.PROJECT_ID.val,
                "redirect_uris": [GoogleAdsApiKeys.REDIRECT_URIS.val],
                "developer_token": GoogleAdsApiKeys.DEVELOPER_TOKEN.val,
                "auth_uri": 'https://accounts.google.com/o/oauth2/auth',
                "token_uri": 'https://accounts.google.com/o/oauth2/token',
                "auth_provider_x509_cert_url": 'https://www.googleapis.com/oauth2/v1/certs'
            }
        }

    @staticmethod
    def get_dict_credentials(client_refresh_token):
        """ Возвращает словарь с конфигурацией подключения к API для GoogleAdsClient"""
        return {
            "developer_token": GoogleAdsApiKeys.DEVELOPER_TOKEN.val,
            "refresh_token": client_refresh_token,
            "client_id": GoogleAdsApiKeys.CLIENT_ID.val,
            "client_secret": GoogleAdsApiKeys.CLIENT_SECRET.val,
            "use_proto_plus": True
        }

    @staticmethod
    def get_scopes():
        return [
            'openid',
            'https://www.googleapis.com/auth/adwords',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile'
        ]


class SMTPParams(__AppGroupConfig):
    """ Параметры подключения к почте для отправки писем"""
    URL = 'SMTP_URL', 'smtp.yandex.com'
    PORT = 'SMTP_PORT', 465
    LOGIN = 'SMTP_LOGIN'
    PASSWORD = 'SMTP_PASSWORD'

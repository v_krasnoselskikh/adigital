from logging import getLogger
from datetime import datetime, timedelta
from app.models import OAuthKeysModel, AccountsModel
from app.types.systems import NameSystemType

CREDENTIALS_LOGGER = getLogger('Credentials')


class Credentials:
    """
    Класс отвечающий за Оauth ключи в приложении

    """

    def __init__(self, account_id):
        """ При инициализации подгрузит модели с авторизацией"""

        self.__key_model: OAuthKeysModel = OAuthKeysModel.get(account_id=account_id)
        # Обновить ключ, если он закончился.
        if self.is_expires_key():
            self.update_refresh_token()

    @property
    def access_token(self):
        return self.__key_model.access_token

    @property
    def refresh_token(self):
        return self.__key_model.refresh_token

    @property
    def system_id(self):
        return self.__key_model.system.id

    @property
    def account_login(self):
        return self.__key_model.account_id

    @staticmethod
    def create_or_update(
            system_id: int,
            account_id: str,
            access_token: str = None,
            refresh_token: str = None,
            token_uri: str = None,
            scopes: str = None,
            expires_in: datetime = None,
            **kwargs
    ):
        """ Создаст или обновит токен в БД"""
        account = AccountsModel.get_or_none(system_id=system_id, account_id=account_id)
        assert account is not None, f'Не найден аккаунт {account_id}'
        key, created = OAuthKeysModel.get_or_create(system_id=system_id, account=account)

        credentials = Credentials(key.account_id)
        credentials.update_token_info(
            access_token,
            refresh_token,
            token_uri,
            scopes,
            expires_in
        )

        return credentials

    def is_expires_key(self) -> bool:
        """ Проверка, что срок ключа еще не кончился. Если ключ кончился, метод вернет True.
         Если у ключа не было заполнено время окончания, метод вернет False.
         """
        if self.__key_model.expires_in:
            if self.__key_model.expires_in < datetime.utcnow():
                CREDENTIALS_LOGGER.warning('Требуется обновить ключ для аккаунта')
                return True
        return False

    def update_token_info(self,
                          access_token: str = None,
                          refresh_token: str = None,
                          token_uri: str = None,
                          scopes: str = None,
                          expires_in: datetime = None, **kwargs):
        """ Обновить параметры токена. Все параметры опциональные, не обязательно передавать"""
        if access_token:
            self.__key_model.access_token = access_token
        if refresh_token:
            self.__key_model.refresh_token = refresh_token
        if token_uri:
            self.__key_model.token_uri = token_uri
        if scopes:
            self.__key_model.scopes = scopes
        if expires_in:
            assert type(expires_in) is datetime, "Тип expires_in должен быть в datetime"
            self.__key_model.expires_in = expires_in
        self.__key_model.save()

    def update_refresh_token(self):
        """ Обновить токен c помощью Refresh токена"""
        if NameSystemType(self.system_id) is NameSystemType.YandexDirect:
            from app.api.direct.auth import get_new_access_token
            new_token = get_new_access_token(self.refresh_token)
            self.update_token_info(**new_token)

        # Для Google ключи обновляются самостоятельно

        CREDENTIALS_LOGGER.info(f'Обновлен токен для {self.__key_model.account_id}')

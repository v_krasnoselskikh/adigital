from datetime import datetime

from app.types import NameSystemType, EntityForCheck
from app.models import db, CampaignsModel, ClientsModel


class Campaign(EntityForCheck):
    def __init__(self, system: NameSystemType = None, campaign_id=None, _model=None):
        super().__init__()
        if campaign_id is None:
            assert _model is not None, 'Не был передан параметр campaign_id'
        self.__model: CampaignsModel = _model if _model else CampaignsModel.get(
            campaign_id=campaign_id, system=system.id)

    @property
    def id(self):
        return self.__model.campaign_id

    @property
    def system_type(self):
        return NameSystemType(self.__model.system_id)

    @property
    def name(self):
        return self.__model.campaign_name

    @property
    def account_login(self):
        return self.__model.client_to_account.account_id

    @property
    def client_login(self):
        return self.__model.client_id

    @property
    def info(self):
        return self.__model.info

    @property
    def active_status(self):
        if str(self.__model.status).lower() in ('enabled', 'paused'):
            return True
        return False

    @staticmethod
    def create_or_get(system: NameSystemType, campaign_id, account_id, client_id):
        campaign, created = CampaignsModel.get_or_create(system=system.id,
                                                         account_id=account_id,
                                                         client_id=client_id,
                                                         campaign_id=campaign_id)
        return Campaign(system, campaign.campaign_id)

    def update_info(self, campaign_name: str = None, client_id: str = None, status: str = None,
                    info: dict = None):
        """Обновляет кампанию"""
        if campaign_name:
            self.__model.campaign_name = campaign_name

        if client_id:
            self.__model.client_id = client_id

        if status:
            self.__model.status = status

        if info:
            self.__model.info = info

        self.__model.save()

    @staticmethod
    def create_many(campaigns: list) -> list:
        """Создает кампании в БД по идентификаторам

        :param campaigns: список групп объявлений в формате dict({
            'key': '3_1',
            'system_id': 3,
            'client_id': 'client_1',
            'campaign_id': 1,
            'campaign_name': 'Campaign Name',
            'status': 'Status',
            'last_updated': '2020-01-01 00:00:00',
            'info': {}
        })
        :return: list: ключи добавленных кампаний
        """
        # Проверяем какие кампании уже добавлены в БД
        campaign_keys = [campaign['key'] for campaign in campaigns]
        existing_campaigns = (CampaignsModel
                              .select(CampaignsModel.key)
                              .where(CampaignsModel.key.in_(campaign_keys)))

        existing_campaign_keys = [campaign.key for campaign in existing_campaigns]
        # Добавляем кампании, которых еще нет в БД.
        rows = [
            CampaignsModel(**{
                **campaign,
                'client_key': ClientsModel.gen_key(campaign['system_id'], campaign['client_id'])
            })
            for campaign in campaigns
            if campaign['key'] not in existing_campaign_keys
        ]
        with db.atomic():
            CampaignsModel.bulk_create(rows, batch_size=50)

        return [row.key for row in rows]

    @staticmethod
    def create_and_update_many(campaigns: list):
        """Создает и обновляет информацию о кампаниях

        :param campaigns: список кампаний в формате dict({
            'system_id': 3,
            'client_id': 'client_1',
            'campaign_id': 1,
            'campaign_name': 'Campaign Name',
            'status': 'Status',
            'info': {}
        })
        """
        if not len(campaigns):
            return

        last_updated = datetime.utcnow()
        for campaign in campaigns:
            # Ключ кампании.
            campaign['key'] = CampaignsModel.gen_key(campaign['system_id'], campaign['campaign_id'])
            # Дата обновления данных.
            campaign['last_updated'] = last_updated

        added_campaign_keys = Campaign.create_many(campaigns)
        rows = [CampaignsModel(**campaign)
                for campaign in campaigns
                if campaign['key'] not in added_campaign_keys]
        with db.atomic():
            CampaignsModel.bulk_update(rows,
                                       fields=['campaign_name', 'status', 'info', 'last_updated'],
                                       batch_size=50)

    def check_campaign_budget(self):
        """Проверяет наличие ограничения по бюджету"""
        if self.system_type is NameSystemType.YandexDirect:
            from app.api.direct.rules import campaign_budget
            return campaign_budget(self.info)
        elif self.system_type is NameSystemType.GoogleAds:
            from app.api.google_ads.rules import campaign_budget
            return campaign_budget(self.id)

    def check_campaign_strategy(self):
        """Проверяет наличие автостратегии"""
        if self.system_type is NameSystemType.YandexDirect:
            from app.api.direct.rules import campaign_strategy
            return campaign_strategy(self.info)
        elif self.system_type is NameSystemType.GoogleAds:
            from app.api.google_ads.rules import campaign_strategy
            return campaign_strategy(self.info)

import flask
from google.oauth2.credentials import Credentials
import googleapiclient.discovery
from config import GoogleOAuthKey


def build_credentials():
    oauth2_tokens = flask.session[GoogleOAuthKey.AUTH_TOKEN_KEY.val]

    return Credentials(
        oauth2_tokens['access_token'],
        refresh_token=oauth2_tokens['refresh_token'],
        client_id=GoogleOAuthKey.CLIENT_ID.val,
        client_secret=GoogleOAuthKey.CLIENT_SECRET.val,
        token_uri=GoogleOAuthKey.ACCESS_TOKEN_URI.val)


def get_user_info():
    credentials = build_credentials()
    oauth2_client = googleapiclient.discovery.build('oauth2', 'v2', cache_discovery=False, credentials=credentials)
    return oauth2_client.userinfo().get().execute()

from datetime import datetime
from logging import getLogger
from app.types import NameSystemType, AccountType
from app.models import AccountsModel
from .credentials import Credentials
from logging_config import ROOT_LOGGER


class Account:
    """ Контроллер отвечающий за действия с аккаунтами"""

    def __init__(self, account_id):
        self.__model: AccountsModel = AccountsModel.get(account_id=account_id)
        self.logger = ROOT_LOGGER.getChild('Account').getChild(str(self.__model.account_id))

    @property
    def system_type(self) -> NameSystemType:
        return NameSystemType(self.__model.system_id)

    @property
    def account_login(self):
        return self.__model.account_id

    @property
    def id(self):
        return self.__model.account_id

    @property
    def name(self):
        return self.__model.account_name

    @property
    def info(self):
        return self.__model.info

    @property
    def system_id(self):
        return self.__model.system_id

    @property
    def account_type(self):
        return self.__model.account_type

    @staticmethod
    def create_or_update(
            system: NameSystemType,
            account_id: str,
            account_name: str,
            project_id: int,
            account_type: AccountType = None,
            info: dict = None,
            **kwargs
    ):
        """ Создаст новый или обновит Аккаунт в БД"""
        account, created = AccountsModel.get_or_create(system_id=system.id, account_id=account_id, project_id=project_id)
        acc = Account(account.account_id)
        acc.update_account_info(account_name=account_name, account_type=account_type, info=info)
        return acc

    def update_account_info(self,
                            account_name: str = None,
                            account_type: AccountType = None,
                            last_updated: datetime = None,
                            info: dict = None):
        if account_name:
            self.__model.account_name = account_name

        if account_type:
            self.__model.account_type = account_type.value

        if last_updated:
            self.__model.last_updated = last_updated

        if info:
            self.__model.info = info

        self.__model.save()

    def set_credential(self,
                       access_token: str = None,
                       refresh_token: str = None,
                       token_uri: str = None,
                       scopes: str = None,
                       expires_in: datetime = None,
                       **kwargs
                       ):
        """ Установит и запишет доступы к аккаунту в БД"""

        # Имеем 2 случая:
        # - Аккаунт только создан, и не имеет доступов -> нужно записать доступы
        # - Аккаунт уже ранее подключался, прилетели новые доступы -> нужно перезаписать текущие
        keys = [key for key in self.__model.oauth_key]

        assert len(keys) in [0, 1], 'Аккаунт имеет более одного ключа.' \
                                    ' Требуется вручную удалить неиспоьземый ключ из базы'

        Credentials.create_or_update(
            system_id=self.system_id,
            account_id=self.id,
            access_token=access_token,
            refresh_token=refresh_token,
            token_uri=token_uri,
            scopes=scopes,
            expires_in=expires_in
        )

    def save_clients(self):
        """Сохраняет и обновляет данные по клиентам"""
        try:
            if self.system_type is NameSystemType.YandexDirect:
                from app.api.direct.method import save_clients
                save_clients(self.id)
            elif self.system_type is NameSystemType.GoogleAds:
                from app.api.google_ads.method import save_clients
                from app.api.google_ads.api import get_client_by_google_account_id
                client = get_client_by_google_account_id(self.id)
                save_clients(client, self.id)
            else:
                raise AssertionError(f'Неизвестная система {self.system_type}')
            self.update_account_info(last_updated=datetime.utcnow())
        except Exception as e:
            self.logger.exception(f'Не удалось обновить аккаунт {self.name} из-за ошибки {e.args}')

    def delete(self):
        """Удаляет все данные об аккаунте"""
        self.__model.delete_instance(recursive=True)

from datetime import datetime

from app.types import NameSystemType
from app.models import db, AdGroupsModel, CampaignsModel
from app.types.entity_for_check import EntityForCheck


class AdGroup(EntityForCheck):

    def __init__(self, system: NameSystemType = None, ad_group_id=None, _model: AdGroupsModel = None):
        super().__init__()
        if ad_group_id is None:
            assert _model is not None, 'Не был передан параметр ad_group_id'
            self.__model: AdGroupsModel = _model

        if ad_group_id is not None:
            self.__model: AdGroupsModel = AdGroupsModel.get(key=AdGroupsModel.gen_key(system.id, ad_group_id))

    @property
    def campaign(self):
        return self.__model.campaign_id

    @property
    def id(self):
        return self.__model.ad_group_id

    @property
    def system_type(self):
        return NameSystemType(self.__model.system_id)

    @property
    def name(self):
        return self.__model.ad_group_name

    @property
    def account_login(self):
        return self.__model.client_to_account.account_id

    @property
    def client_login(self):
        return self.__model.client_id

    @property
    def info(self):
        return self.__model.info

    @property
    def active_status(self):
        if self.__model.status == 'ENABLED' or self.__model.status is None:
            return True
        return False

    @staticmethod
    def create_or_get(system: NameSystemType, ad_group_id, campaign_id):
        ad_group, created = AdGroupsModel.get_or_create(system=system.id,
                                                        ad_group_id=ad_group_id,
                                                        campaign_id=campaign_id)
        return AdGroup(system, ad_group.ad_group_id)

    def update_info(self, ad_group_name: str = None, campaign_id: int = None, status: str = None, info: dict = None):
        """ Обновляет группу объявлений"""
        if ad_group_name:
            self.__model.ad_group_name = ad_group_name

        if campaign_id:
            self.__model.campaign_id = campaign_id

        if status:
            self.__model.status = status

        if info:
            self.__model.info = info

        self.__model.save()

    @staticmethod
    def create_many(ad_groups: list) -> list:
        """Создает группы объявлений в БД по идентификаторам

        :param ad_groups: список групп объявлений в формате dict({
            'key': '3_1',
            'system_id': 3,
            'client_id': 'client_1',
            'campaign_id': 1,
            'ad_group_id': 1,
            'ad_group_name': 'Ad Group Name',
            'status': 'Status',
            'last_updated': '2020-01-01 00:00:00',
            'info': {}
        })
        :return: list: ключи добавленных групп объявлений
        """
        # Проверяем какие группы объявлений уже добавлены в БД
        ad_group_keys = [ad_group['key'] for ad_group in ad_groups]
        existing_ad_groups = (AdGroupsModel
                              .select(AdGroupsModel.key)
                              .where(AdGroupsModel.key.in_(ad_group_keys)))

        existing_ad_group_keys = [ad_group.key for ad_group in existing_ad_groups]
        # Добавляем группы объявлений, которых еще нет в БД
        rows = [
            AdGroupsModel(**{
                **ad_group,
                'campaign_key': CampaignsModel.gen_key(ad_group['system_id'], ad_group['campaign_id'])
            })
            for ad_group in ad_groups
            if ad_group['key'] not in existing_ad_group_keys
        ]
        with db.atomic():
            AdGroupsModel.bulk_create(rows, batch_size=500)

        return [row.key for row in rows]

    @staticmethod
    def create_and_update_many(ad_groups: list):
        """Создает и обновляет информацию о группах объявлений

        :param ad_groups: список групп объявлений в формате dict({
            'system_id': 3,
            'client_id': 'client_1',
            'campaign_id': 1,
            'ad_group_id': 1,
            'ad_group_name': 'Ad Group Name',
            'status': 'Status',
            'info': {}
        })
        """
        if not len(ad_groups):
            return

        last_updated = datetime.utcnow()
        for ad_group in ad_groups:
            # Ключ группы.
            ad_group['key'] = AdGroupsModel.gen_key(ad_group['system_id'], ad_group['campaign_id'], ad_group['ad_group_id'])
            # Дата обновления данных.
            ad_group['last_updated'] = last_updated

        added_ad_group_keys = AdGroup.create_many(ad_groups)
        rows = [AdGroupsModel(**ad_group)
                for ad_group in ad_groups
                if ad_group['key'] not in added_ad_group_keys]
        with db.atomic():
            AdGroupsModel.bulk_update(rows,
                                      fields=['ad_group_name', 'status', 'info', 'last_updated'],
                                      batch_size=50)

from uuid import uuid4
from playhouse.shortcuts import model_to_dict
from app.models import ProjectsModel, ProjectToUser, UserModel, DoesNotExist, DATETIME_FORMAT


class Project:
    def __init__(self, project_id=None, _model=None):
        self.__model = _model if _model else ProjectsModel.get_by_id(project_id)

    def get_id(self):
        return self.__model.id

    @staticmethod
    def get_user_project(user_id):
        user = UserModel.get(id=user_id)
        try:
            project = user.projects.get().project
        except DoesNotExist:
            project = ProjectsModel.create(name=uuid4().node)
            ProjectToUser.create(user=user, project=project)

        return Project(_model=project)

    def get_info(self):
        """ Выдает информацию о проекте"""
        info = model_to_dict(self.__model, only=[
            ProjectsModel.id,
            ProjectsModel.name,
            ProjectsModel.register_date
        ])

        info['register_date'] = info['register_date'].strftime('%d-%m-%Y %H:%M:%S')
        return info

from datetime import datetime

from app.types import NameSystemType, EntityForCheck
from app.models import db, AdsModel, AdGroupsModel


class Ad(EntityForCheck):
    def __init__(self, system: NameSystemType = None, ad_group_id=None, ad_id=None, _model: AdsModel = None):
        super().__init__()
        if ad_group_id is None or ad_id is None:
            assert _model is not None, 'Не был передан параметр ad_group_id или ad_id '

        self.__model: AdsModel = _model if _model else AdsModel.get(
            ad_id=ad_id, ad_group_id=ad_group_id, system=system.id)

    @property
    def id(self):
        return self.__model.ad_id

    @property
    def system_type(self):
        return NameSystemType(self.__model.system_id)

    @property
    def name(self):
        return self.__model.ad_name

    @property
    def account_login(self):
        return self.__model.client_to_account.account_id

    @property
    def client_login(self):
        return self.__model.client_id

    @property
    def info(self):
        return self.__model.info

    @property
    def campaign(self):
        return self.__model.campaign_id

    @property
    def ad_group(self):
        return self.__model.ad_group_id

    @property
    def active_status(self):
        if self.__model.status in ('ENABLED', 'ON'):
            return True
        return False

    @staticmethod
    def create_or_get(system: NameSystemType, ad_group_id, ad_id):
        ad, created = AdsModel.get_or_create(system=system.id,
                                             ad_group_id=ad_group_id,
                                             ad_id=ad_id)
        return Ad(system, ad.ad_group_id, ad.ad_id)

    def update_info(self, ad_name: str = None, ad_group_id: int = None, status: str = None, info: dict = None):
        """Обновляет объявление"""
        if ad_name:
            self.__model.ad_name = ad_name

        if ad_group_id:
            self.__model.ad_group_id = ad_group_id

        if status:
            self.__model.status = status

        if info:
            self.__model.info = info

        self.__model.save()

    @staticmethod
    def create_many(ads: list) -> list:
        """Создает объявления в БД по идентификаторам

        :param ads: список объявлений для обновления в формате dict({
            'key': '3_1_1',
            'system_id': 3,
            'client_id': 'client_1',
            'campaign_id': 1,
            'ad_group_id': 1,
            'ad_id': 1,
            'ad_name': 'Ad Name',
            'status': 'Status',
            'last_updated': '2020-01-01 00:00:00',
            'info': {}
        })
        :return: list: ключи добавленных объявлений
        """
        # Проверяем какие объявления уже добавлены в БД
        ad_keys = [ad['key'] for ad in ads]
        existing_ads = (AdsModel
                        .select(AdsModel.key)
                        .where(AdsModel.key.in_(ad_keys)))

        existing_ad_keys = [ad.key for ad in existing_ads]
        # Добавляем объявления, которых еще нет в БД
        rows = [
            AdsModel(**{
                **ad,
                'ad_group_key': AdGroupsModel.gen_key(ad['system_id'], ad['campaign_id'], ad['ad_group_id']),
            })
            for ad in ads
            if ad['key'] not in existing_ad_keys
        ]
        with db.atomic():
            AdsModel.bulk_create(rows, batch_size=50)

        return [row.key for row in rows]

    @staticmethod
    def create_and_update_many(ads: list):
        """Создает и обновляет информацию об объявлениях

        :param ads: список объявлений для обновления в формате dict({
            'system_id': 3,
            'client_id': 'client_1',
            'campaign_id': 1,
            'ad_group_id': 1,
            'ad_id': 1,
            'ad_name': 'Ad Name',
            'status': 'Status',
            'info': {}
        })
        """
        if not len(ads):
            return

        last_updated = datetime.utcnow()
        for ad in ads:
            # Ключ объявления.
            ad['key'] = AdsModel.gen_key(ad['system_id'], ad['ad_group_id'], ad['ad_id'])
            # Дата обновления данных.
            ad['last_updated'] = last_updated

        added_ad_keys = Ad.create_many(ads)
        rows = [AdsModel(**ad)
                for ad in ads
                if ad['key'] not in added_ad_keys]
        with db.atomic():
            AdsModel.bulk_update(rows,
                                 fields=['ad_name', 'status', 'info', 'last_updated'],
                                 batch_size=50)

    def check_ad_utm(self):
        """Проверяет разметку"""
        if self.system_type is NameSystemType.YandexDirect:
            from app.api.direct.rules import ad_utm
            return ad_utm(self.info)
        elif self.system_type is NameSystemType.GoogleAds:
            from app.api.google_ads.rules import ad_utm
            return ad_utm(self.info)

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from config import SMTPParams



def send_email(
        subject='Уведомление с сервиса asur-adigital.ru',
        to='',
        content='',
        reply_to=SMTPParams.LOGIN.val
):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = f'asur-adigital.ru <{SMTPParams.LOGIN.val}>'
    msg['To'] = to
    msg['Reply-to'] = reply_to

    part1 = MIMEText(content, 'html')
    msg.attach(part1)

    server = smtplib.SMTP_SSL(SMTPParams.URL.val, SMTPParams.PORT.val)
    server.login(SMTPParams.LOGIN.val, SMTPParams.PASSWORD.val)
    addresses = [m.replace(' ', '') for m in msg['To'].split(',') if len(m) > 1]
    # send message:
    server.sendmail(SMTPParams.LOGIN.val, addresses, msg.as_string())
    # close connection
    server.quit()

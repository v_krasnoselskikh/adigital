from datetime import datetime

from .campaigns import Campaign
from .ad_groups import AdGroup
from .ads import Ad
from app.types import NameSystemType, CheckResult
from app.models import db, ClientsModel, ClientToAccountModel, CampaignsModel, AdGroupsModel, AdsModel
from logging_config import ROOT_LOGGER


class Client:
    def __init__(self, client_id=None, _model: ClientsModel = None):
        if client_id is None:
            assert _model is not None, 'Не был передан параметр client_id'

        self.__model: ClientsModel = _model if _model else ClientsModel.get(client_id=client_id)
        self.logger = ROOT_LOGGER.getChild('Client').getChild(str(self.__model.client_id))

    @property
    def id(self):
        return self.__model.client_id

    @property
    def system_type(self):
        return NameSystemType(self.__model.system_id)

    @property
    def name(self):
        return self.__model.client_name

    @property
    def account_login(self):
        return self.__model.client_to_account.account_id

    @property
    def info(self):
        return self.__model.info

    @staticmethod
    def create_or_get(system: NameSystemType, client_id, account_id):
        client, created = ClientsModel.get_or_create(system=system.id, client_id=client_id)
        if created:
            ClientToAccountModel.get_or_create(system_id=system.id, account_id=account_id, client_id=client_id)

        return Client(client_id=client.client_id)

    def update_info(self,
                    client_name: str = None,
                    status: str = None,
                    last_updated: datetime = None,
                    last_checked: datetime = None,
                    info: dict = None):
        """Обновляет клиента"""
        if client_name:
            self.__model.client_name = client_name

        if status:
            self.__model.status = status

        if last_updated:
            self.__model.last_updated = last_updated

        if last_checked:
            self.__model.last_checked = last_checked

        if info:
            self.__model.info = info

        self.__model.save()

    @staticmethod
    def create_many(system: NameSystemType, account_id, clients: list):
        """Создает клиентов в БД по идентификаторам

        :param system: система
        :param account_id: логин аккаунта
        :param clients: клиенты в формате dict({
            'key': '3_client_1',
            'system_id': 3,
            'account_id': 'account',
            'client_id': 'client_1',
            'client_name': 'Client Name',
            'status': 'Status',
            'info': {}
        })
        """
        # Проверяем какие клиенты уже добавлены в БД.
        client_keys = [client['key'] for client in clients]
        existing_clients = (ClientsModel
                            .select(ClientsModel.key)
                            .where(ClientsModel.key.in_(client_keys)))
        existing_client_keys = [client.key for client in existing_clients]
        # Добавляем клиентов, которых еще нет в БД.
        ClientsModel.insert_many([
            {
                'key': client['key'],
                'system_id': client['system_id'],
                'client_id': client['client_id'],
            }
            for client in clients
            if client['key'] not in existing_client_keys
        ]).execute()

        # Проверяем какие клиенты к аккаунту уже привязаны в БД.
        existing_clients_to_account = (ClientToAccountModel
                                       .select(ClientToAccountModel.client_key)
                                       .where(ClientToAccountModel.client_key.in_(client_keys),
                                              ClientToAccountModel.account_id == account_id))

        existing_clients_to_account_keys = [client.client_key for client in existing_clients_to_account]
        # Добавляем клиентов, которых еще нет у аккаунта в БД.
        ClientToAccountModel.insert_many([
            {
                'client_key': client_key,
                'account_id': account_id
            }
            for client_key in client_keys
            if client_key not in existing_clients_to_account_keys
        ]).execute()

    @staticmethod
    def create_and_update_many(system: NameSystemType, account_id, clients: list):
        """Создает и обновляет информацию о клиентах

        :param system: система
        :param account_id: логин аккаунта
        :param clients: список клиентов в формате dict({
            'account_id': 'account',
            'client_id': 'client_1',
            'client_name': 'Client Name',
            'status': 'Status',
            'info': {}
        })
        """
        if not len(clients):
            return

        # Ключи клиентов.
        for client in clients:
            client['system_id'] = system.id
            client['key'] = ClientsModel.gen_key(system.id, client['client_id'])

        Client.create_many(system, account_id, clients)
        rows = [ClientsModel(**client) for client in clients]
        with db.atomic():
            ClientsModel.bulk_update(rows,
                                     fields=['client_name', 'status', 'info'],
                                     batch_size=50)

    def save_all_data(self):
        """Сохраняет и обновляет данные по кампаниям, группам объявлений, объявлениям"""
        if self.system_type is NameSystemType.YandexDirect:
            from app.api.direct.method import save_all
            save_all(self.id)
        elif self.system_type is NameSystemType.GoogleAds:
            from app.api.google_ads.method import save_all
            save_all(self.account_login)
        else:
            raise AssertionError(f'Неизвестная система {self.system_type}')
        # Обновляем дату обновления данных клиента
        self.update_info(last_updated=datetime.utcnow())

    def check(self):
        """Проверяет клиента на наличие ошибок"""
        last_checked = datetime.now()
        # Обновляем данные в одной транзакции
        with db.atomic() as txn:
            # Кампании
            for campaign_model in self.__model.campaigns:
                campaign = Campaign(_model=campaign_model)
                if campaign.active_status:
                    campaign_model.has_budget = campaign.check_campaign_budget() == CheckResult.SUCCESSFUL
                    campaign_model.has_strategy = campaign.check_campaign_strategy() == CheckResult.SUCCESSFUL
                    campaign_model.has_utm = True
                    campaign_model.last_checked = last_checked
                    campaign_model.save()
                    # Группы объявлений
                    for ad_group_model in campaign_model.ad_groups:
                        ad_group = AdGroup(_model=ad_group_model)
                        if ad_group.active_status:
                            # Объявления
                            for ad_model in ad_group_model.ads:
                                ad = Ad(_model=ad_model)
                                if ad.active_status:
                                    has_utm = ad.check_ad_utm() == CheckResult.SUCCESSFUL
                                    ad_model.has_utm = has_utm
                                    # Если хотя бы у одного объявления неправильная разметка,
                                    # то у кампании ставим флаг неправильная разметка
                                    if not has_utm:
                                        campaign_model.has_utm = has_utm
                                        campaign_model.save()
                                    ad_model.last_checked = last_checked
                                    ad_model.save()
            self.__model.last_checked = last_checked
            self.__model.save()
            txn.commit()

    def save_and_check(self):
        """Обновлянт данные и проверяет клиента на наличие ошибок"""
        try:
            self.save_all_data()
            self.check()
        except Exception as e:
            self.logger.error(f'Не удалось обновить клиента {self.name} из-за ошибки {e.args}')


    def delete(self):
        """Удаляет все данные о клиенте"""
        self.__model.delete_instance(recursive=True)

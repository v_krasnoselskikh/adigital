import logging
from os import getenv
from datetime import date, datetime
from apscheduler.schedulers.background import BackgroundScheduler

from app.controllers.clients import Client
from app.controllers.accounts import Account
from app.models import ClientsModel, AccountsModel


LOGGER_SCHEDULER = logging.getLogger('APScheduler')


def update_clients():
    """Обновление данных о клиентах (загрузка и проверка данных о кампаниях, группах объявлений, обяъвлениях)"""
    clients = (ClientsModel
               .select(ClientsModel.client_id)
               # .where(ClientsModel.last_updated.is_null() or
               #        ClientsModel.last_updated.day != date.today())
               )
    clients_ids = [client.client_id for client in clients]
    LOGGER_SCHEDULER.info(f'Список клиентов для обновления {clients_ids}')
    for client_id in clients_ids:
        LOGGER_SCHEDULER.info(f'Начало загрузки и проверки клиента {client_id}')
        client = Client(client_id)
        client.save_and_check()
        LOGGER_SCHEDULER.info(f'Конец загрузки и проверки клиента {client_id}')


def update_accounts():
    """Обновление данных об аккаунтах (загрузка списка клиентов)"""
    accounts = (AccountsModel
                .select(AccountsModel.account_id)
                .where(AccountsModel.last_updated.is_null() or
                       AccountsModel.last_updated.day != date.today())
                )
    accounts_ids = [account.account_id for account in accounts]
    LOGGER_SCHEDULER.info(f'Список аккаунтов для обновления {accounts_ids}')
    for account_id in accounts_ids:
        LOGGER_SCHEDULER.info(f'Начало загрузки аккаунта {account_id}')
        account = Account(account_id)
        account.save_clients()
        LOGGER_SCHEDULER.info(f'Конец загрузка аккаунта {account_id}')


job_defaults = {
    'coalesce': False,
    'max_instances': 2
}
scheduler = BackgroundScheduler(job_defaults=job_defaults)

if getenv('DISABLE_TASK') != 'True':
    scheduler.add_job(update_clients, 'interval', id='update_clients', hours=1, next_run_time=datetime.now())
    scheduler.add_job(update_accounts, 'interval', id='update_accounts', hours=12, next_run_time=datetime.now())

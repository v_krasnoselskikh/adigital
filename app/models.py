from peewee import *
from playhouse.postgres_ext import ForeignKeyField
from playhouse.pool import PooledPostgresqlExtDatabase
from playhouse.postgres_ext import BinaryJSONField
from datetime import date, datetime
from flask_login import UserMixin

from config import DataBaseConfig

db = PooledPostgresqlExtDatabase(
    DataBaseConfig.DATABASE.val,
    user=DataBaseConfig.USER.val,
    password=DataBaseConfig.PASSWORD.val,
    host=DataBaseConfig.HOST.val,
    port=DataBaseConfig.PORT.val,
    max_connections=100
)

DATETIME_FORMAT = 'DD-MM-YYYY HH24:MI:SS'

class BaseModel(Model):
    class Meta:
        database = db


class UserModel(BaseModel, UserMixin):
    id = CharField(primary_key=True)
    name = CharField(null=True)
    email = CharField(null=True)
    is_admin = BooleanField(default=False)
    is_active = BooleanField(default=True)
    register_date = DateTimeField(null=True, default=lambda: datetime.now())

    class Meta:
        db_table = 'users'


class ProjectsModel(BaseModel):
    name = CharField(null=True)
    register_date = DateTimeField(null=True, default=lambda: datetime.now())

    class Meta:
        db_table = 'projects'


class ProjectToUser(BaseModel):
    user = ForeignKeyField(UserModel, backref='projects')
    project = ForeignKeyField(ProjectsModel, backref='users')

    class Meta:
        db_table = 'projects_to_user'
        primary_key = CompositeKey('user', 'project')


class SystemsModel(BaseModel):
    """ Ид Систем"""
    name = CharField()

    class Meta:
        db_table = 'systems'


class AccountsModel(BaseModel):
    """ Аккаунты """
    system = ForeignKeyField(SystemsModel)
    project = ForeignKeyField(ProjectsModel)
    account_id = CharField(null=True, primary_key=True)
    account_name = CharField(null=True)
    account_type = CharField(null=True, verbose_name='Тип аккаунта (клиентский\агентский)')
    created = DateTimeField(default=lambda: datetime.utcnow())
    last_updated = DateTimeField(null=True)
    info = BinaryJSONField(null=True)

    class Meta:
        db_table = 'accounts'


class OAuthKeysModel(BaseModel):
    """ Ключи Oauth авторизаций"""
    system = ForeignKeyField(SystemsModel)
    account = ForeignKeyField(AccountsModel, backref='oauth_key', primary_key=True)
    access_token = TextField(null=True)
    refresh_token = TextField(null=True)
    token_uri = CharField(null=True)
    scopes = CharField(null=True)
    expires_in = DateTimeField(null=True)
    created = DateTimeField(default=lambda: datetime.utcnow())

    class Meta:
        db_table = 'oauth_keys'


class ClientsModel(BaseModel):
    """ Клиенты"""
    key = CharField(primary_key=True)
    system = ForeignKeyField(SystemsModel)
    client_id = CharField()
    client_name = TextField(null=True)
    status = TextField(null=True)
    info = BinaryJSONField(null=True)
    created = DateTimeField(default=lambda: datetime.utcnow())
    last_updated = DateTimeField(null=True)
    last_checked = DateTimeField(null=True)

    class Meta:
        db_table = 'clients'

    @staticmethod
    def gen_key(system_id, client_id):
        return f'{system_id}_{client_id}'

    @property
    def accounts(self):
        return ClientToAccountModel.select().where(
            ClientToAccountModel.system_id == self.system_id,
            ClientToAccountModel.client_id == self.client_id
        )

    @property
    def client_to_account(self):
        return ClientToAccountModel.get(client_key=ClientsModel.gen_key(self.system, self.client_id))


class ClientToAccountModel(BaseModel):
    """ Связывающая таблица Клиента и Аккаунт
    Один и тот же клиент может быть на разных аккаунтах
    """
    account = ForeignKeyField(AccountsModel, backref='clients')
    client = ForeignKeyField(ClientsModel, column_name='client_key', backref='accounts')

    class Meta:
        db_table = 'client_to_account'
        primary_key = CompositeKey('account', 'client')


class CampaignsModel(BaseModel):
    """ Кампании """
    key = CharField(primary_key=True)
    system = ForeignKeyField(SystemsModel)
    client = ForeignKeyField(ClientsModel, column_name='client_key', backref='campaigns')
    client_id = CharField()
    campaign_id = BigIntegerField(verbose_name='Идентификатор кампании')
    campaign_name = TextField(null=True, verbose_name='Название кампании')
    status = CharField(null=True, verbose_name='Состояние кампании: ARCHIVED, CONVERTED, ENDED, OFF, ON, SUSPENDED')
    info = BinaryJSONField(null=True)
    has_budget = BooleanField(null=True)
    has_strategy = BooleanField(null=True)
    has_utm = BooleanField(null=True)
    created = DateTimeField(default=lambda: datetime.utcnow())
    last_updated = DateTimeField(null=True)
    last_checked = DateTimeField(null=True)

    @staticmethod
    def gen_key(system_id, campaign_id):
        return f'{system_id}_{campaign_id}'

    class Meta:
        db_table = 'campaigns'

    @property
    def client_to_account(self):
        return ClientToAccountModel.get(client_key=ClientsModel.gen_key(self.system, self.client_id))


class AdGroupsModel(BaseModel):
    """ Группы объявлений"""
    key = CharField(primary_key=True)
    system = ForeignKeyField(SystemsModel)
    campaign = ForeignKeyField(CampaignsModel, column_name='campaign_key', backref='ad_groups')
    client_id = CharField()
    campaign_id = BigIntegerField()
    ad_group_id = BigIntegerField(verbose_name='Идентификатор группы объявлений')
    ad_group_name = TextField(null=True, verbose_name='Название группы объявлений')
    status = CharField(null=True, verbose_name='Состояние группы объявлений')
    info = BinaryJSONField(null=True)
    created = DateTimeField(default=lambda: datetime.utcnow())
    last_updated = DateTimeField(null=True)
    last_checked = DateTimeField(null=True)

    @staticmethod
    def gen_key(system_id, campaign_id, ad_group_id):
        return f'{system_id}_{campaign_id}_{ad_group_id}'

    class Meta:
        db_table = 'ad_groups'

    @property
    def client_to_account(self):
        return ClientToAccountModel.get(client_key=ClientsModel.gen_key(self.system, self.client_id))


class AdsModel(BaseModel):
    """ Объявления"""
    key = CharField(primary_key=True)
    system = ForeignKeyField(SystemsModel)
    ad_group = ForeignKeyField(AdGroupsModel, column_name='ad_group_key', backref='ads')
    client_id = CharField()
    campaign_id = BigIntegerField()
    ad_group_id = BigIntegerField()
    ad_id = BigIntegerField(verbose_name='Идентификатор объявления')
    ad_name = TextField(null=True, verbose_name='Название объявления')
    status = CharField(null=True, verbose_name='Состояние объявления: OFF, ON, SUSPENDED, OFF_BY_MONITORING, ARCHIVED')
    info = BinaryJSONField(null=True)
    has_utm = BooleanField(null=True)
    created = DateTimeField(default=lambda: datetime.utcnow())
    last_updated = DateTimeField(null=True)
    last_checked = DateTimeField(null=True)

    class Meta:
        db_table = 'ads'

    @staticmethod
    def gen_key(system_id, ad_group_id, ad_id):
        return f'{system_id}_{ad_group_id}_{ad_id}'

    @property
    def client_to_account(self):
        return ClientToAccountModel.get(client_key=ClientsModel.gen_key(self.system, self.client_id))

class GoogleCampaignBudgetModel(BaseModel):
    resource_name = CharField(primary_key=True)
    campaign = ForeignKeyField(CampaignsModel, column_name='campaign_key', null=True)
    client = ForeignKeyField(ClientsModel, column_name='client_key', null=True)
    delivery_method= CharField(null=True)
    budget_id = CharField(null=True)
    budget_type= CharField(null=True)
    name= CharField(null=True)
    period= CharField(null=True)
    status= CharField(null=True)
    amount = FloatField(null=True)
    total_amount= FloatField(null=True)

    class Meta:
        db_table = 'google_budget_campaigns'


def create_tables():
    UserModel.create_table(safe=True)
    ProjectsModel.create_table(safe=True)
    ProjectToUser.create_table(safe=True)
    SystemsModel.create_table(safe=True)
    AccountsModel.create_table(safe=True)
    OAuthKeysModel.create_table(safe=True)
    ClientsModel.create_table(safe=True)
    ClientToAccountModel.create_table(safe=True)
    CampaignsModel.create_table(safe=True)
    AdGroupsModel.create_table(safe=True)
    AdsModel.create_table(safe=True)
    GoogleCampaignBudgetModel.create_table(safe=True)

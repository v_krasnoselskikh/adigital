from abc import ABC, abstractmethod
from app.types import NameSystemType
from config import ROOT_LOGGER


class EntityForCheck(ABC):
    """ Абстрактный класс для Объектов [Кампании, Групы  объявлений, Объявления].
    Реализует интерфейс
    """

    def __init__(self):
        self.__logger = ROOT_LOGGER.getChild(self.entity_name())
        self._result_rules_check = {}

    @property
    @abstractmethod
    def system_type(self) -> NameSystemType:
        """ Возвращает тип системы"""
        pass

    @property
    @abstractmethod
    def account_login(self):
        """ Возвращает первый аккаунт, к которому привязана сущность"""
        pass

    @property
    @abstractmethod
    def client_login(self):
        """ Возвращает логин/id клиента, к которому привязана сущность"""
        pass

    @property
    @abstractmethod
    def info(self):
        """ Json Ответ от Api. В дальнейшем этот ответ нужно передать как есть в функции с правилами"""
        pass

    @classmethod
    def entity_name(cls):
        """ Возвращает названия класса сущности"""
        return cls.__name__

    @property
    def logger(self):
        return self.__logger

    @property
    @abstractmethod
    def active_status(self):
        """ Возвращает True, если сущность активна"""
        pass

from enum import Enum


class AccountType(Enum):
    Agency = 'Agency'
    Client = 'Client'

import logging
from enum import Enum


class CheckResult(Enum):
    """ Тип, который должен возвращаться как результат работы функии проверки"""
    SUCCESSFUL = 1      # Успешно
    NOT_SUCCESSFUL = 0  # Неуспешно, ошибка
    SKIPPED = None      # Правило было пропущено

    def get_logging_level(self):
        """ каждый из результатов имеет свой уровень в logging"""
        logging_level = {
            self.SUCCESSFUL: logging.INFO,
            self.NOT_SUCCESSFUL: logging.WARNING,
            self.SKIPPED: logging.DEBUG
        }
        return logging_level.get(self)



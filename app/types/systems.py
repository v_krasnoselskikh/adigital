from enum import Enum
from app.models import SystemsModel


class NameSystemType(Enum):
    """ Объект отвечающий за ID систем в базе данных"""
    YandexDirect = 1
    GoogleAds = 2

    def __init__(self, id_system):
        self.id = id_system

    @classmethod
    def create_in_db(cls):
        """Создет в бд Записи о системах"""

        db_names = {
            'YandexDirect': 'Яндекс Директ',
            'GoogleAds': 'Google Ads'
        }

        for e in cls:
            e: NameSystemType = e
            system, created = SystemsModel.get_or_create(id=e.id,
                                                         name=db_names.get(e.name, e.name))


YandexDirect = NameSystemType.YandexDirect
GoogleAds = NameSystemType.GoogleAds

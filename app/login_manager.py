from flask_login import LoginManager

from app.models import UserModel, DoesNotExist

login_manager = LoginManager()
login_manager.login_view = 'login.login'
login_manager.login_message = "Войдите в ваш аккаунт, чтобы просматривать содержание этой страницы"


@login_manager.user_loader
def load_user(user_id):
    try:
        user = UserModel.get(id=user_id)
        if user.is_active:
            return user
    except DoesNotExist:
        pass

    return None

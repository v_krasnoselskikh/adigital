import flask
import json

from flask import jsonify, request, redirect, flash
from flask_login import current_user, login_required

from ..api.direct.auth import by_pass_code_and_save_credentials as direct_save_credentials
from app.api.direct.auth import authorize_link as direct_authorize_link

app_url = flask.Blueprint('direct', __name__)


@app_url.route('/')
@login_required
def direct_login():
    link = direct_authorize_link()
    return redirect(link, 302)


@app_url.route('/oauth')
@login_required
def direct_callback():
    code = request.values.get('code')

    if code is None:
        flash('Не был передан code от сервера Yandex Direct')
        return redirect('/', code=302)

    try:
        result = direct_save_credentials(code, current_user)
        message = 'Ключи авторизации Direct к аккаунту был получены.'
        flash(message)
    except ConnectionError as e:
        error_info = e.args[1]
        flash(str(error_info))

    return redirect('/', code=302)

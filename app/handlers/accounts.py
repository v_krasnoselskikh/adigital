import flask

from flask import jsonify
from flask_login import current_user, login_required
from threading import Thread

from app.models import AccountsModel, ClientsModel, ClientToAccountModel, SystemsModel, fn, DATETIME_FORMAT
from app.controllers.accounts import Account
from app.controllers.projects import Project

app_url = flask.Blueprint('accounts', __name__)


@app_url.route('/')
@login_required
def get_accounts():
    project = Project.get_user_project(current_user.id)
    accounts = list(AccountsModel
                    .select(AccountsModel.account_id,
                            SystemsModel.name.alias('system'),
                            AccountsModel.account_name,
                            AccountsModel.account_type,
                            fn.to_char(AccountsModel.last_updated, DATETIME_FORMAT).alias('last_updated'),
                            fn.to_char(AccountsModel.created, DATETIME_FORMAT).alias('created'))
                    .where(AccountsModel.project_id == project.get_id())
                    .join(SystemsModel)
                    .dicts())
    return jsonify(accounts)


@app_url.route('/<account_id>/save_clients')
@login_required
def save_clients(account_id):
    account = Account(account_id)
    thread = Thread(target=account.save_clients)
    thread.daemon = True
    thread.start()
    return jsonify({'thread_name': str(thread.name),
                    'started': True})


@app_url.route('/<account_id>/clients')
@login_required
def get_clients(account_id):
    clients = list(ClientsModel
                   .select(ClientsModel.key,
                           ClientsModel.client_id,
                           ClientsModel.client_name,
                           SystemsModel.name.alias('system'),
                           ClientsModel.status,
                           fn.to_char(ClientsModel.last_updated, DATETIME_FORMAT).alias('last_updated'),
                           fn.to_char(ClientsModel.last_checked, DATETIME_FORMAT).alias('last_checked'),
                           fn.to_char(ClientsModel.created, DATETIME_FORMAT).alias('created'),
                           ClientToAccountModel.account)
                   .join(ClientToAccountModel)
                   .join_from(ClientsModel, SystemsModel)  # Для ClientsModel добавляем справочник SystemsModel
                   .where(ClientToAccountModel.account == account_id)
                   .dicts())
    return jsonify(clients)


@app_url.route('/<account_id>', methods=["DELETE"])
@login_required
def delete(account_id):
    account = Account(account_id)
    account.delete()
    return jsonify(dict(result=True))

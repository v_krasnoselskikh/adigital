from flask import Blueprint, request, flash, redirect
from flask_login import current_user, login_required
from app.api.google_ads.auth import save_credentials, authorize_link

app_url = Blueprint('google_ads', __name__)


@app_url.route('/')
@login_required
def adwords_login():
    link = authorize_link()
    return redirect(link)


@app_url.route('/oauth2callback')
@login_required
def adwords_callback():
    authorization_response = request.url
    try:
        result = save_credentials(authorization_response, current_user)
        message = 'Ключи авторизации Google ADS к аккаунту был получены.'
        flash(message)
    except Exception as e:
        flash(str(e.args), 'error')

    return redirect('/', code=302)

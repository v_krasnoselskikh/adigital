import flask
from typing import Literal, Optional, TypedDict
from dataclasses import dataclass, asdict
from flask import jsonify, request

from app.controllers.email import send_email

app_url = flask.Blueprint('email', __name__)


class RequestForConnection(TypedDict):
    name: str
    phone: str
    email: str


@dataclass(frozen=True)
class EmailResponse:
    status: Literal['ok', 'error']
    message: Optional[str] = None
    field: Optional[str] = None


@app_url.route('request-for-connection', methods=['POST'])
def request_for_connection():
    r = request.get_json()
    r = RequestForConnection(**r)

    if r.get('name') is None:
        res = asdict(EmailResponse(status='error', message='Не заполнено поле ФИО'))
        return jsonify(res), 400

    if r.get('email') is None:
        return jsonify(asdict(EmailResponse(status='error', message='Не заполнено поле Email'))), 400

    if r.get('phone') is None:
        return jsonify(asdict(EmailResponse(status='error', message='Не заполнено поле телефон'))), 400

    content = f"""
    <p>Пользователь оставил заявку на подключение</p>
    <p></p>
    <p>ФИО: {r['name']}</p>
    <p>Телефон: {r['phone']}</p>    
    <p>Email: {r['email']} </p>
    <p></p>
    ------------------    
    """

    send_email(
        subject="АСУР: заявка на подключение",
        to="info@asur-adigital.ru",
        content=content
    )

    return jsonify(EmailResponse(status='ok', message='Сообщение отправлено'))

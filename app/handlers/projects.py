from flask import Blueprint, jsonify
from flask_login import login_required, current_user
from app.controllers import Project

projects_blueprint = Blueprint('projects', __name__)


@projects_blueprint.route('/')
@login_required
def index():
    project = Project.get_user_project(current_user.id)
    info = project.get_info()
    return jsonify(info)

import flask

from itertools import groupby
from flask import jsonify, redirect, url_for
from flask_login import current_user, login_required
from threading import Thread

from app.models import ClientsModel, ClientToAccountModel, CampaignsModel, AdGroupsModel, AdsModel, SystemsModel, \
    fn, DATETIME_FORMAT
from app.controllers.clients import Client

app_url = flask.Blueprint('clients', __name__)


@app_url.route('/')
@login_required
def get_clients():
    clients = list(ClientsModel
                   .select(ClientsModel.key,
                           SystemsModel.name.alias('system'),
                           ClientsModel.client_id,
                           ClientsModel.client_name,
                           ClientsModel.status,
                           fn.to_char(ClientsModel.last_updated, DATETIME_FORMAT).alias('last_updated'),
                           fn.to_char(ClientsModel.last_checked, DATETIME_FORMAT).alias('last_checked'),
                           fn.to_char(ClientsModel.created, DATETIME_FORMAT).alias('created'),
                           ClientToAccountModel.account)
                   .join(ClientToAccountModel)
                   .join_from(ClientsModel, SystemsModel)  # Для ClientsModel добавляем справочник SystemsModel
                   .dicts())
    return jsonify(clients)


@app_url.route('/<client_id>', methods=["GET"])
@login_required
def index(client_id):
    """Возврщает список кампаний с данными о группах объявлений и объявлениях в формате {
        'campaign_id': 1,
        'campaign_name': 'Campaign Name 1',
        'ad_groups': [
            'ad_group_id': 2,
            'ad_group_name': 'Ad Group Name 2',
            'ads': [
                'ad_id': 3,
                'ad_name': 'Ad Name 3',
            ]
        ]
    }"""
    # Словарь компаний
    campaigns = list(CampaignsModel
                     .select(CampaignsModel.campaign_id,
                             CampaignsModel.campaign_name,
                             SystemsModel.name.alias('system'),
                             CampaignsModel.status,
                             CampaignsModel.has_budget,
                             CampaignsModel.has_strategy,
                             CampaignsModel.has_utm,
                             fn.to_char(CampaignsModel.last_updated, DATETIME_FORMAT).alias('last_updated'),
                             fn.to_char(CampaignsModel.last_checked, DATETIME_FORMAT).alias('last_checked'),
                             fn.to_char(CampaignsModel.created, DATETIME_FORMAT).alias('created'))
                     .join_from(CampaignsModel, SystemsModel)  # Для CampaignsModel добавляем справочник SystemsModel
                     .where(CampaignsModel.client_id == client_id).dicts())
    campaigns = {campaign['campaign_id']: campaign for campaign in campaigns}

    # Словарь групп объявлений
    ad_groups = list(AdGroupsModel
                     .select(AdGroupsModel.ad_group_id,
                             AdGroupsModel.ad_group_name,
                             AdGroupsModel.campaign_id,
                             SystemsModel.name.alias('system'),
                             AdGroupsModel.status,
                             fn.to_char(AdGroupsModel.last_updated, DATETIME_FORMAT).alias('last_updated'),
                             fn.to_char(AdGroupsModel.last_checked, DATETIME_FORMAT).alias('last_checked'),
                             fn.to_char(AdGroupsModel.created, DATETIME_FORMAT).alias('created'))
                     .join_from(AdGroupsModel, SystemsModel)  # Для AdGroupsModel добавляем справочник SystemsModel
                     .where(AdGroupsModel.client_id == client_id).dicts())
    ad_groups = {ad_group['ad_group_id']: ad_group for ad_group in ad_groups}

    # Список объявлений
    ads = list(AdsModel
               .select(AdsModel.ad_id,
                       AdsModel.ad_name,
                       AdsModel.campaign_id,
                       AdsModel.ad_group_id,
                       SystemsModel.name.alias('system'),
                       AdsModel.status,
                       AdsModel.has_utm,
                       fn.to_char(AdsModel.last_updated, DATETIME_FORMAT).alias('last_updated'),
                       fn.to_char(AdsModel.last_checked, DATETIME_FORMAT).alias('last_checked'),
                       fn.to_char(AdsModel.created, DATETIME_FORMAT).alias('created'))
               .join_from(AdsModel, SystemsModel)  # Для AdsModel добавляем справочник SystemsModel
               .where(AdsModel.client_id == client_id).dicts())

    # Группируем объявлениям по 'ad_group_id' и добавляем в словарь групп объявлений
    ads.sort(key=lambda x: x['ad_group_id'])
    for key, values in groupby(ads, key=lambda x: x['ad_group_id']):
        if key in ad_groups:
            ad_groups[key]['ads'] = list(values)

    # Группируем группы объявлений по 'campaign_id' и добавляем в словарь кампаний
    ad_groups_list = list(ad_groups.values())
    ad_groups_list.sort(key=lambda x: x['campaign_id'])
    for key, values in groupby(ad_groups_list, key=lambda x: x['campaign_id']):
        if key in campaigns:
            campaigns[key]['ad_groups'] = list(values)

    return jsonify(dict(result=campaigns))


@app_url.route('/<client_id>/check')
@login_required
def client_check(client_id):
    client = Client(client_id)
    thread = Thread(target=client.save_and_check())
    thread.daemon = True
    thread.start()
    return jsonify({'thread_name': str(thread.name),
                    'started': True})


@app_url.route('/<client_id>/campaigns')
@login_required
def get_campaigns(client_id):
    campaigns = list(CampaignsModel
                     .select(CampaignsModel.campaign_id,
                             CampaignsModel.campaign_name,
                             SystemsModel.name.alias('system'),
                             CampaignsModel.status,
                             CampaignsModel.has_budget,
                             CampaignsModel.has_strategy,
                             CampaignsModel.has_utm,
                             fn.to_char(CampaignsModel.last_updated, DATETIME_FORMAT).alias('last_updated'),
                             fn.to_char(CampaignsModel.last_checked, DATETIME_FORMAT).alias('last_checked'),
                             fn.to_char(CampaignsModel.created, DATETIME_FORMAT).alias('created'))
                     .join_from(CampaignsModel, SystemsModel)  # Для CampaignsModel добавляем справочник SystemsModel
                     .where(CampaignsModel.client_id == client_id).dicts())
    return jsonify(dict(result=campaigns))


@app_url.route('/<client_id>/ad_groups')
@login_required
def get_ad_groups(client_id):
    ad_groups = list(AdGroupsModel
                     .select(AdGroupsModel.ad_group_id,
                             AdGroupsModel.ad_group_name,
                             AdGroupsModel.campaign_id,
                             SystemsModel.name.alias('system'),
                             AdGroupsModel.status,
                             fn.to_char(AdGroupsModel.last_updated, DATETIME_FORMAT).alias('last_updated'),
                             fn.to_char(AdGroupsModel.last_checked, DATETIME_FORMAT).alias('last_checked'),
                             fn.to_char(AdGroupsModel.created, DATETIME_FORMAT).alias('created'))
                     .join_from(AdGroupsModel, SystemsModel)  # Для AdGroupsModel добавляем справочник SystemsModel
                     .where(AdGroupsModel.client_id == client_id).dicts())
    return jsonify(dict(result=ad_groups))


@app_url.route('/<client_id>/ads')
@login_required
def get_ads(client_id):
    ads = list(AdsModel
               .select(AdsModel.ad_id,
                       AdsModel.ad_name,
                       AdsModel.campaign_id,
                       AdsModel.ad_group_id,
                       SystemsModel.name.alias('system'),
                       AdsModel.status,
                       AdsModel.has_utm,
                       fn.to_char(AdsModel.last_updated, DATETIME_FORMAT).alias('last_updated'),
                       fn.to_char(AdsModel.created, DATETIME_FORMAT).alias('last_checked'),
                       fn.to_char(AdsModel.created, DATETIME_FORMAT).alias('created'))
               .join_from(AdsModel, SystemsModel)  # Для AdsModel добавляем справочник SystemsModel
               .where(AdsModel.client_id == client_id).dicts())
    return jsonify(dict(result=ads))


@app_url.route('/<client_id>', methods=["DELETE"])
@login_required
def delete(client_id):
    client = Client(client_id)
    client.delete()
    return jsonify(dict(result=True))

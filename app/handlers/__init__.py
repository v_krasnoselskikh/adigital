import flask

from flask import Flask, redirect, request, jsonify, get_flashed_messages, session
from flask_login import current_user

from .login import login_blueprint
from .direct import app_url as direct
from .google_ads import app_url as adwords
from .accounts import app_url as accounts
from .clients import app_url as clients
from .projects import projects_blueprint as projects
from .email import app_url as emails


def register_blueprints(app: Flask):
    app.register_blueprint(login_blueprint, url_prefix='/google')
    app.register_blueprint(direct, url_prefix='/direct')
    app.register_blueprint(adwords, url_prefix='/api/adwords')
    app.register_blueprint(accounts, url_prefix='/accounts')
    app.register_blueprint(clients, url_prefix='/clients')
    app.register_blueprint(projects, url_prefix='/projects')
    app.register_blueprint(emails, url_prefix='/email')

    @app.errorhandler(404)
    def page_not_found(e):
        # note that we set the 404 status explicitly
        return jsonify(message='Страница не найдена'), 404

    @app.errorhandler(500)
    def page_not_found(e):
        # note that we set the 404 status explicitly
        return jsonify(error='Внутрення ошибка сервера'), 500

    @app.route('/')
    @app.route('/login')
    @app.route('/offer/tariffs')
    @app.route('/documentation')
    @app.route('/licence')
    @app.route('/contacts')
    @app.route('/licence')
    @app.route('/confidential')
    @app.route('/personal-data-consent')
    @app.route('/basic-rules')
    @app.route('/company')
    def page_without_auth():
        return app.send_static_file('dist/index.html')

    @app.route('/account')
    @app.route('/account/<account>')
    @app.route('/account/<account>/client/<client>')
    @app.route('/settings')
    def index(account=None, client=None):
        if current_user.is_anonymous:
            return redirect('/login')
        return app.send_static_file('dist/index.html')

    @app.route('/get_flashed_messages')
    def get_messages():
        messages = get_flashed_messages(with_categories='True')
        if messages is None:
            return jsonify([])
        session.pop("_flashes", None)
        return jsonify(
            [dict(category=category if category else 'info', message=message) for category, message in messages])

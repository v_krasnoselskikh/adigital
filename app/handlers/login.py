import functools

from authlib.integrations.requests_client import OAuth2Session
from flask import Blueprint, make_response, jsonify, redirect, request, session
from flask_login import login_user, logout_user, current_user
from playhouse.shortcuts import model_to_dict
from app.models import UserModel
from config import GoogleOAuthKey
from ..controllers.google_auth import get_user_info

login_blueprint = Blueprint('login', __name__)


def no_cache(view):
    @functools.wraps(view)
    def no_cache_impl(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    return functools.update_wrapper(no_cache_impl, view)


@login_blueprint.route('/login')
@no_cache
def login():
    oauth_session = OAuth2Session(GoogleOAuthKey.CLIENT_ID.val,
                                  GoogleOAuthKey.CLIENT_SECRET.val,
                                  scope=GoogleOAuthKey.AUTHORIZATION_SCOPE.val,
                                  redirect_uri=GoogleOAuthKey.AUTH_REDIRECT_URI.val)

    uri, state = oauth_session.create_authorization_url(GoogleOAuthKey.AUTHORIZATION_URL.val)

    session[GoogleOAuthKey.AUTH_STATE_KEY.val] = state
    session.permanent = True

    return redirect(uri, code=302)


@login_blueprint.route('/auth')
@no_cache
def google_auth_redirect():
    req_state = request.args.get('state', default=None, type=None)

    if GoogleOAuthKey.AUTH_STATE_KEY.val not in session:
        return jsonify(error='Invalid AUTH_STATE'), 401

    if req_state != session[GoogleOAuthKey.AUTH_STATE_KEY.val]:
        return jsonify(error='Invalid state parameter'), 401

    oauth_session = OAuth2Session(GoogleOAuthKey.CLIENT_ID.val,
                                  GoogleOAuthKey.CLIENT_SECRET.val,
                                  scope=GoogleOAuthKey.AUTHORIZATION_SCOPE.val,
                                  state=req_state,
                                  redirect_uri=GoogleOAuthKey.AUTH_REDIRECT_URI.val)

    oauth2_tokens = oauth_session.fetch_access_token(
        GoogleOAuthKey.ACCESS_TOKEN_URI.val,
        authorization_response=request.url
    )

    session[GoogleOAuthKey.AUTH_TOKEN_KEY.val] = oauth2_tokens

    # Проверяем есть ли пользователь в базе
    user_info = get_user_info()
    user_db, created = UserModel.get_or_create(id=user_info['id'])
    if created:
        user_db.name = user_info['name']
        user_db.email = user_info['email']
        user_db.save()

    login_user(user_db)

    return redirect('/', code=302)


@login_blueprint.route('/user_info')
def user_info():
    if current_user.is_anonymous:
        return jsonify(is_login=False)

    user_info_db = model_to_dict(current_user, only=[
        UserModel.id,
        UserModel.name,
        UserModel.email,
        UserModel.is_admin,
        UserModel.is_active,
        UserModel.register_date
    ])
    return jsonify(is_login=True, user_info=user_info_db)


@login_blueprint.route('/logout')
@no_cache
def logout():
    logout_user()
    session.clear()
    return jsonify(is_login=False)

from typing import List, Optional

from google.ads.googleads.client import GoogleAdsClient
from app.controllers import Client, Campaign, AdGroup, Ad
from app.types import NameSystemType
from logging_config import ROOT_LOGGER
from app.models import GoogleCampaignBudgetModel, CampaignsModel, ClientsModel
from .api import get_client_by_google_account_id
from .types import AccountInformation, CampaignInfo, AdGroupInfo, AdInfo, GoogleCampaignBudget

LOGGER = ROOT_LOGGER.getChild('GoogleADS')


def micros_to_currency(micros) -> Optional[float]:
    return micros / 1000000.0 if micros is not None else None


def get_list_accessible_accounts(client: GoogleAdsClient) -> List[str]:
    """ 
    https://developers.google.com/google-ads/api/docs/account-management/listing-accounts
    """

    customer_service = client.get_service("CustomerService")
    accessible_customers = customer_service.list_accessible_customers()
    return [str(r).replace('customers/', '') for r in accessible_customers.resource_names]


def get_account_information(client: GoogleAdsClient, customer_id) -> AccountInformation:
    """ Получаем информацию об аккаунтах

    https://github.com/googleads/google-ads-python/blob/main/examples/account_management/get_account_information.py
    """
    ga_service = client.get_service("GoogleAdsService")
    query = """
            SELECT
                customer.id,
                customer.resource_name,
                customer.descriptive_name,
                customer.currency_code,
                customer.time_zone
            FROM customer
            LIMIT 1"""

    request = client.get_type("SearchGoogleAdsRequest")
    request.customer_id = customer_id
    request.query = query
    response = ga_service.search(request=request)
    customer = list(response)[0].customer

    return {
        "customer_id": customer.id,
        "resource_name": customer.resource_name,
        "descriptive_name": customer.descriptive_name,
        "currency_code": customer.currency_code,
        "time_zone": customer.time_zone
    }


def get_campaigns_budget(client: GoogleAdsClient, customer_id) -> List[GoogleCampaignBudget]:
    ga_service = client.get_service("GoogleAdsService")

    query = """
            SELECT
                campaign.id,
                campaign_budget.amount_micros,
                campaign_budget.delivery_method,
                campaign_budget.id,
                campaign_budget.name,
                campaign_budget.period,
                campaign_budget.reference_count,
                campaign_budget.resource_name,
                campaign_budget.status,
                campaign_budget.total_amount_micros,
                campaign_budget.type
            FROM campaign_budget"""

    stream = ga_service.search_stream(customer_id=customer_id, query=query)
    result = []

    for batch in stream:
        for row in batch.results:
            data: GoogleCampaignBudget = dict(
                resource_name=row.campaign_budget.resource_name,
                campaign_id=row.campaign.id,
                delivery_method=row.campaign_budget.delivery_method.name,
                budget_id=row.campaign_budget.id,
                budget_type=row.campaign_budget.type.name,
                name=row.campaign_budget.name,
                period=row.campaign_budget.period.name,
                status=row.campaign_budget.status.name,
                amount=micros_to_currency(row.campaign_budget.amount_micros),
                total_amount=micros_to_currency(row.campaign_budget.total_amount_micros)
            )
            result.append(data)
    return result


def get_campaigns(client: GoogleAdsClient, customer_id: str) -> List[CampaignInfo]:
    """Возвращает список кампаний клиента

    :param client: Google Api
    :param customer_id: id клиента
    :return: : id клиента
    """

    ga_service = client.get_service("GoogleAdsService")

    query = """
                SELECT
                  campaign.id,
                  campaign.name,
                  campaign.status,
                  campaign.url_custom_parameters,
                  campaign.accessible_bidding_strategy  
                  
                FROM campaign
                ORDER BY campaign.id"""

    # Issues a search request using streaming.
    stream = ga_service.search_stream(customer_id=customer_id, query=query)

    result = []
    for batch in stream:
        for row in batch.results:
            resp: CampaignInfo = dict(
                campaign_id=row.campaign.id,
                campaign_name=row.campaign.name,
                status=row.campaign.status.name,
                url_custom_parameters=row.campaign.url_custom_parameters,
                bidding_strategy=row.campaign.accessible_bidding_strategy
            )
            result.append(resp)
    return result


def get_ad_groups(client: GoogleAdsClient, customer_id: str) -> List[AdGroupInfo]:
    """Возвращает список групп объявлений кампании

    :param client: Google Api
    :param customer_id: id клиента
    """
    ga_service = client.get_service("GoogleAdsService")

    query = f"""
           SELECT
            campaign.id,
            ad_group.id,
            ad_group.name,
            ad_group.status
           FROM ad_group
        """

    stream = ga_service.search_stream(customer_id=customer_id, query=query)

    result = []
    for batch in stream:
        for row in batch.results:
            result.append({
                "campaign_id": row.campaign.id,
                "ad_group_id": row.ad_group.id,
                "ad_group_name": row.ad_group.name,
                "status": row.ad_group.status.name,
            })
    return result


def get_ads(client: GoogleAdsClient, customer_id: str) -> List[AdInfo]:
    """Возвращает список объявлений группы объявлений

    :param client: Google Api
    :param customer_id: id клиента
    """
    ga_service = client.get_service("GoogleAdsService")

    query = f"""
               SELECT
                campaign.id,
                ad_group.id,
                ad_group_ad.status,
                ad_group_ad.ad.id,
                ad_group_ad.ad.name,                
                ad_group_ad.ad.type,
                ad_group_ad.ad.display_url,
                ad_group_ad.ad.final_urls,
                ad_group_ad.ad.tracking_url_template
               FROM ad_group_ad 
               WHERE ad_group_ad.status != REMOVED 
            """

    stream = ga_service.search_stream(customer_id=customer_id, query=query)

    results = []

    # Iterate over all ads in all rows returned and count disapproved ads.
    for batch in stream:
        results.extend([{
            "campaign_id": row.campaign.id,
            "ad_group_id": row.ad_group.id,
            "ad_id": row.ad_group_ad.ad.id,
            "ad_name": row.ad_group_ad.ad.name,
            "status": row.ad_group_ad.status.name,
            "display_url": row.ad_group_ad.ad.display_url,
            "final_urls": row.ad_group_ad.ad.final_urls,
            "type_ad": row.ad_group_ad.ad.type.name,
            "tracking_url_template": row.ad_group_ad.ad.tracking_url_template
        }
            for row in batch.results])

    return results


# save methods


def save_clients(client: GoogleAdsClient, account_id):
    """Сохраняет список всех доступных рекламных кабинетов, доступных для аккаунта    """
    result = []
    for customer_id in get_list_accessible_accounts(client=client):
        info = get_account_information(client=client, customer_id=customer_id)

        result.append({
            'client_id': info['customer_id'],
            'client_name': info['descriptive_name'],
            'info': info
        })
    Client.create_and_update_many(NameSystemType.GoogleAds, account_id, result)


def save_campaigns(client: GoogleAdsClient):
    """ Сохраняет список кампаний клиента """
    result = []
    for customer_id in get_list_accessible_accounts(client=client):
        camp = get_campaigns(client=client, customer_id=customer_id)

        result.extend([
            {
                'system_id': NameSystemType.GoogleAds.id,
                'client_id': customer_id,
                'campaign_id': c['campaign_id'],
                'campaign_name': c['campaign_name'],
                'status': c['status'],
                'info': {
                    "url_custom_parameters": list(c['url_custom_parameters']),
                    "bidding_strategy": c['bidding_strategy']
                }
            } for c in camp])

    Campaign.create_and_update_many(result)


def save_ad_groups(client: GoogleAdsClient):
    """Сохраняет список групп объявлений кампании"""

    result = []
    for customer_id in get_list_accessible_accounts(client=client):
        ad_groups_source = get_ad_groups(client, customer_id=customer_id)
        ad_groups = [
            {
                'system_id': NameSystemType.GoogleAds.id,
                'client_id': customer_id,
                'campaign_id': ad_group_source['campaign_id'],
                'ad_group_id': ad_group_source['ad_group_id'],
                'ad_group_name': ad_group_source['ad_group_name'],
                'status': ad_group_source['status'],
                'info': {}
            }
            for ad_group_source in ad_groups_source
        ]
        result.extend(ad_groups)
    AdGroup.create_and_update_many(ad_groups)


def save_ads(client: GoogleAdsClient):
    """ Сохраняет список объявлений """
    # Загружаем справочник группа объявлений - кампания, чтобы сохранить объявления с campaign_id
    result = []
    for customer_id in get_list_accessible_accounts(client=client):
        ad_source = get_ads(client, customer_id=customer_id)
        ads = [
            {
                'system_id': NameSystemType.GoogleAds.id,
                'client_id': customer_id,
                'campaign_id': ad['campaign_id'],
                'ad_group_id': ad['ad_group_id'],
                'ad_id': ad['ad_id'],
                'ad_name': ad['ad_name'],
                'status': ad['status'],
                'info': {
                    'final_urls': list(ad['final_urls']),
                    'type_ad': ad['type_ad'],
                    'tracking_url_template': ad['tracking_url_template'],
                    'display_url': ad['display_url'],
                }
            }
            for ad in ad_source
        ]

        result.extend(ads)
    Ad.create_and_update_many(result)


def save_google_campaigns_budget(client: GoogleAdsClient):
    for customer_id in get_list_accessible_accounts(client=client):
        budget = get_campaigns_budget(client=client, customer_id=customer_id)

        result = [dict(
            resource_name=r['resource_name'],
            campaign=CampaignsModel.gen_key(2, r['campaign_id']),
            client=ClientsModel.gen_key(2, customer_id),
            delivery_method=r['delivery_method'],
            budget_id=r['budget_id'],
            budget_type=r['budget_type'],
            name=r['name'],
            period=r['period'],
            status=r['status'],
            amount=r['amount'],
            total_amount=r['total_amount'],
        ) for r in budget]

        GoogleCampaignBudgetModel.delete().where(
            [GoogleCampaignBudgetModel.client == ClientsModel.gen_key(2, customer_id)]).execute()
        GoogleCampaignBudgetModel.insert_many(result, fields=[
            GoogleCampaignBudgetModel.resource_name,
            GoogleCampaignBudgetModel.campaign,
            GoogleCampaignBudgetModel.client,
            GoogleCampaignBudgetModel.delivery_method,
            GoogleCampaignBudgetModel.budget_id,
            GoogleCampaignBudgetModel.budget_type,
            GoogleCampaignBudgetModel.name,
            GoogleCampaignBudgetModel.period,
            GoogleCampaignBudgetModel.status,
            GoogleCampaignBudgetModel.amount,
            GoogleCampaignBudgetModel.total_amount
        ]).execute()


def save_all(account_id):
    """Сохраняет все кампании, группы объявлений, объявления клиента

    :param account_id: логин клиента
    """
    client = get_client_by_google_account_id(account_id)
    save_clients(client, account_id)
    save_campaigns(client)
    save_ad_groups(client)
    save_ads(client)
    save_google_campaigns_budget(client)

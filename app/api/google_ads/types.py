from typing import TypedDict, Optional


class AccountInformation(TypedDict):
    customer_id: str
    resource_name: str
    descriptive_name: str
    currency_code: str
    time_zone: str


class CampaignInfo(TypedDict):
    campaign_id: str
    campaign_name: str
    status: str
    url_custom_parameters: str
    bidding_strategy: str


class GoogleCampaignBudget(TypedDict):
    resource_name: str
    campaign_id: str
    delivery_method: str
    budget_id: str
    budget_type: str
    name: str
    period: str
    status: str
    amount: Optional[float]
    total_amount: Optional[float]


class AdGroupInfo(TypedDict):
    campaign_id: str
    ad_group_id: str
    ad_group_name: str
    status: str


class AdInfo(TypedDict):
    campaign_id: str
    ad_group_id: str
    ad_id: str
    ad_name: str
    status: str
    display_url: str
    type_ad: str
    final_urls: list
    tracking_url_template: str

from urllib import parse
from app.models import GoogleCampaignBudgetModel, CampaignsModel
from app.types import CheckResult


# rules


def campaign_budget(campaign_id: str) -> CheckResult:
    """Ограничение по бюджету"""
    campaign_key = CampaignsModel.gen_key(2, campaign_id=campaign_id)
    budget: GoogleCampaignBudgetModel = GoogleCampaignBudgetModel.get_or_none(campaign_key=campaign_key)

    if budget and budget.status == 'ENABLED':
        return CheckResult.SUCCESSFUL
    return CheckResult.NOT_SUCCESSFUL


def campaign_strategy(campaign_info: dict) -> CheckResult:
    """ Автостратегии """
    bidding_strategy_resource_name = campaign_info.get('bidding_strategy')

    if not bidding_strategy_resource_name or bidding_strategy_resource_name == '':
        return CheckResult.NOT_SUCCESSFUL
    return CheckResult.SUCCESSFUL


def ad_utm(ad_info: dict) -> CheckResult:
    """ Разметка """
    final_urls = ad_info.get('final_urls', [])
    # Проверяем наличие параметров в ссылках.
    if not len(final_urls):
        return CheckResult.NOT_SUCCESSFUL
    for url in final_urls:
        query = parse.parse_qs(parse.urlparse(url).query)
        params = set(query.keys())
        if not {'utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term'}.issubset(params):
            return CheckResult.NOT_SUCCESSFUL
    # Все ссылки валидны.
    return CheckResult.SUCCESSFUL

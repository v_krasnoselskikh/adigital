import logging
import requests
from google_auth_oauthlib.flow import Flow
from app.controllers import Account, Project
from app.types import NameSystemType
from config import GoogleAdsApiKeys
from .api import get_account_info

LOGGER_AUTH = logging.getLogger('Adwords.Auth')


def authorize_link():
    """Сформирует ссылку на авторизацию приложения

    :return str: Ссылка на страницу авторизации
    """
    flow = Flow.from_client_config(GoogleAdsApiKeys.get_web(),
                                   scopes=GoogleAdsApiKeys.get_scopes())
    flow.redirect_uri = GoogleAdsApiKeys.REDIRECT_URIS.val
    authorization_url, state = flow.authorization_url(access_type='offline', include_granted_scopes='true')
    return authorization_url


def save_credentials(authorization_url, current_user):
    """ Обменяет url авторизации на ключи, сохранит ключи от аккаунта в базу

    :param authorization_url: str
    :param current_user: UserModel
    """
    flow = Flow.from_client_config(GoogleAdsApiKeys.get_web(),
                                   scopes=GoogleAdsApiKeys.get_scopes())
    flow.redirect_uri = GoogleAdsApiKeys.REDIRECT_URIS.val
    flow.fetch_token(authorization_response=authorization_url)
    credentials = flow.credentials

    if not credentials.refresh_token:
        LOGGER_AUTH.error('Ошибка. Не приходит refresh_token. '
                          'Сбрасываем у аккаунта refresh_token для данного приложения.')
        revoke_token(token=credentials.token)
        LOGGER_AUTH.error('Сбросили refresh_token. Попробуйте заново авторизоваться.')
        raise AssertionError('Для авторизации был сброшен refresh_token аккаунта. Повторите авторизацию.')

    account_info = get_account_info(credentials)
    account_params = dict(
        system=NameSystemType.GoogleAds,
        account_id=account_info['id'],
        account_name=account_info['name'],
        info=account_info,
        project_id=Project.get_user_project(current_user.id).get_id()
    )
    account = Account.create_or_update(**account_params)
    account.set_credential(access_token=credentials.token,
                           refresh_token=credentials.refresh_token,
                           token_uri=credentials.token_uri,
                           scopes=credentials.scopes)
    account.save_clients()

    return True


def revoke_token(token):
    """Сброс refresh_token для аккаунта"""
    revoke = requests.post('https://accounts.google.com/o/oauth2/revoke',
                           params={'token': token},
                           headers={'content-type': 'application/x-www-form-urlencoded'})

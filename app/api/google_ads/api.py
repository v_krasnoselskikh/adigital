from google.ads.googleads.client import GoogleAdsClient
from config import GoogleAdsApiKeys
from app.models import AccountsModel, OAuthKeysModel
from googleapiclient.discovery import build

GOOGLE_ADS_VERSION = 'v12'


def get_client_by_refresh_token(refresh_token) -> GoogleAdsClient:
    config_dict = GoogleAdsApiKeys.get_dict_credentials(client_refresh_token=refresh_token)
    return GoogleAdsClient.load_from_dict(config_dict)


def get_client_by_google_account_id(account_id) -> GoogleAdsClient:
    oauth: OAuthKeysModel = OAuthKeysModel.get(account_id=account_id)
    return get_client_by_refresh_token(oauth.refresh_token)


def get_account_info(credentials):
    user_info_service = build('oauth2', 'v2', credentials=credentials)
    user_info = user_info_service.userinfo().get().execute()
    return user_info

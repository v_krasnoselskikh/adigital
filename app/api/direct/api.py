from enum import Enum
import requests
from logging import getLogger
from app.controllers.credentials import Credentials


class YandexDirectApi(Enum):
    """ Api Yandex Direct"""
    authorize = 'https://oauth.yandex.ru/authorize'
    token = 'https://oauth.yandex.ru/token'
    info = 'https://login.yandex.ru/info'
    agency_clients = 'https://api.direct.yandex.com/json/v5/agencyclients'
    clients = 'https://api.direct.yandex.com/json/v5/clients'
    campaigns = 'https://api.direct.yandex.com/json/v5/campaigns'
    ad_groups = 'https://api.direct.yandex.com/json/v5/adgroups'
    ads = 'https://api.direct.yandex.com/json/v5/ads'
    creatives = 'https://api.direct.yandex.com/json/v5/creatives'
    sitelinks = 'https://api.direct.yandex.com/json/v5/sitelinks'
    vcards = 'https://api.direct.yandex.com/json/v5/vcards'

    def __init__(self, url):
        self.url = url
        self.__headers = dict()
        self.__logger = getLogger(__name__)

    @property
    def headers(self):
        return self.__headers

    @headers.setter
    def headers(self, new_value):
        self.__headers = new_value

    def __print_info_about_api_units(self, request_headers):
        """ Считывает и выводит в лог из заголовоков ответа на запрос информацию о баллах Api Direct

        :param request_headers: Словарь заголовков ответа на запрос
        """
        if request_headers.get("Units", False):
            units_response = request_headers.get("Units")
            units_response = units_response.split('/')
            self.__logger.getChild('Units').debug(
                'Информация о баллах \n'
                '   израсходовано при выполнении запроса: %s\n'
                '   доступный остаток: %s\n'
                '   cуточный лимит: %s\n' % (units_response[0], units_response[1], units_response[2]))

    def load_credential_by_account_id(self, account_id):
        """Загрузит access token по account_id"""
        credentials = Credentials(account_id)
        self.__headers.update({
            'Authorization': 'Bearer ' + credentials.access_token
        })

    def load_credential_by_client_login(self, client_login):
        """Загрузит access token по account_id"""
        self.__headers.update({
            'Client-Login': client_login
        })

    def request(self, body: dict, account_id=None, client_login=None, entity_name=None, as_post=False):
        """ Запрос к Api

        :param body: Тело запроса передоваемое в api
        :param account_id: Если указан, то для этого аккаунта будут взяты из базы ключи доступа
        :param client_login: Указывается, если аккаунт, от которого запрашиватся информациия имеет тип Агенства.
        :param entity_name: Указывается, если запрос к определенной сущности.
        :param as_post: Указывается, если нужно отправить переменные запроса (Body) как Post а не JSON.
        """
        self.__headers.update({
            'Accept-Language': 'ru'
        })

        if account_id:
            self.load_credential_by_account_id(account_id)

        if client_login:
            self.load_credential_by_client_login(client_login)

        request_params = dict(
            url=self.url,
            headers=self.__headers
        )
        request_params['data' if as_post else 'json'] = body

        r = requests.post(**request_params)

        self.__print_info_about_api_units(r.headers)

        if r.status_code == 200:
            self.__logger.debug('Пришел ответ на %s' % self.name)
            result = r.json()
            if 'error' in result:
                self.__logger.error(f'Не получен ответ от {self.url} на запрос {body}\nОтвет [{r.status_code}]:{r.text}')
                raise ConnectionError(500, result['error'])
            if 'result' not in result:
                return result

            result = result['result']
            limited = result.get('LimitedBy')
            # Если нет пагинации
            if not limited:
                return result.get(entity_name, result)

            all_result = []
            result = result.get(entity_name)
            all_result += result
            # Если кол-во записей в ответе = максимальному кол-ву, то еще отправляем запрос.
            while limited and limited == len(result):
                request_params['data' if as_post else 'json']['params']['Page'] = {'Offset': len(all_result)}
                r = requests.post(**request_params)
                if r.status_code != 200:
                    break
                result = r.json()
                result = result.get('result', {}).get(entity_name, [])
                all_result += result
                self.__logger.debug('Получено %s записей' % len(all_result))
            else:
                return all_result

        self.__logger.error(f'Не получен ответ от {self.url} на запрос {body}\nОтвет [{r.status_code}]:{r.text}')
        raise ConnectionError(r.status_code, r.text)

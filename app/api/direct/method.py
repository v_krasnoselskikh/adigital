from peewee import chunked

from .api import YandexDirectApi
from app.controllers import Account, Client, Campaign, AdGroup, Ad
from app.types import AccountType, NameSystemType

CAMPAIGN_STATUSES = {
    'CONVERTED': 'Unknown',
    'UNKNOWN': 'Unknown',
    'ON': 'Enabled',
    'ARCHIVED': 'Archived',
    'SUSPENDED': 'Paused',
    'ENDED': 'Paused',
    'OFF': 'Paused'
}


def get_clients(account_login):
    """Возвращает список клиентов агенства. Если аккаунт клиентский, вернется список из одного элемента

    :param account_login: логин аккаунта
    :return: list: список клиентов
    """
    account = Account(account_login)
    fields_for_request = [
        "AccountQuality",
        "Archived",
        "ClientId",
        "ClientInfo",
        "CountryId",
        "CreatedAt",
        "Currency",
        "Grants",
        "Login",
        "Notification",
        "OverdraftSumAvailable",
        "Phone",
        "Representatives",
        "Restrictions",
        "Settings",
        "Type",
        "VatRate"
    ]
    payload = {
        "method": "get",
        "params": {
            "SelectionCriteria": {},
            "FieldNames": fields_for_request,
        }
    }

    if AccountType(account.account_type) is AccountType.Agency:
        clients = YandexDirectApi.agency_clients.request(body=payload, account_id=account_login, entity_name='Clients')
        return clients

    if AccountType(account.account_type) is AccountType.Client:
        del payload['params']['SelectionCriteria']
        clients = YandexDirectApi.clients.request(body=payload, account_id=account_login, entity_name='Clients')
        return clients

    raise AssertionError('Передан не поддерживаемый тип аккаунта -> список кллиентов не может быть получен')


def get_campaigns(client_login):
    """Возвращает список кампаний клиента

    :param client_login: логин клиента
    :return: list: список кампаний
    """
    client = Client(client_login)
    fields_for_request = [
        "BlockedIps",
        "ExcludedSites",
        "Currency",
        "DailyBudget",
        "Notification",
        "EndDate",
        "Funds",
        "ClientInfo",
        "Id",
        "Name",
        "NegativeKeywords",
        "RepresentedBy",
        "StartDate",
        "Statistics",
        "State",
        "Status",
        "StatusPayment",
        "StatusClarification",
        "SourceId",
        "TimeTargeting",
        "TimeZone",
        "Type"
    ]
    payload = {
        "method": "get",
        "params": {
            "SelectionCriteria": {},
            "FieldNames": fields_for_request,
            "TextCampaignFieldNames":
                ["CounterIds", "RelevantKeywords", "Settings", "BiddingStrategy", "PriorityGoals", "AttributionModel"],
            "MobileAppCampaignFieldNames":
                ["Settings","BiddingStrategy"],
            "DynamicTextCampaignFieldNames":
                ["CounterIds", "Settings", "BiddingStrategy", "PriorityGoals", "AttributionModel"],
            "CpmBannerCampaignFieldNames":
                ["CounterIds", "FrequencyCap", "Settings", "BiddingStrategy"],
            "SmartCampaignFieldNames":
                ["CounterId", "Settings", "BiddingStrategy", "PriorityGoals", "AttributionModel"]
        }
    }

    campaigns = YandexDirectApi.campaigns.request(body=payload,
                                                  account_id=client.account_login,
                                                  client_login=client_login,
                                                  entity_name='Campaigns')
    # Убираем дубли.
    campaigns = list({campaign['Id']: campaign for campaign in campaigns}.values())
    return campaigns


def get_ad_groups(campaign_ids: list):
    """Возвращает список групп объявлений кампании

    :param campaign_ids: идентификаторы кампании
    :return: list: список группы объявлений
    """
    if not len(campaign_ids):
        return []
    ad_groups = []
    # Для извлечения аккаунта и клиента берем первую кампанию.
    campaign = Campaign(NameSystemType.YandexDirect, campaign_ids[0])
    for chunk_campaigns in chunked(campaign_ids, 10):
        fields_for_request = [
            "CampaignId",
            "Id",
            "Name",
            "NegativeKeywords",
            "NegativeKeywordSharedSetIds",
            "RegionIds",
            "RestrictedRegionIds",
            "ServingStatus",
            "Status",
            "Subtype",
            "TrackingParams",
            "Type"
        ]
        payload = {
            "method": "get",
            "params": {
                "SelectionCriteria": {
                    'CampaignIds': chunk_campaigns
                },
                "FieldNames": fields_for_request,
                "MobileAppAdGroupFieldNames":
                    ["StoreUrl", "TargetDeviceType", "TargetCarrier", "TargetOperatingSystemVersion",
                     "AppIconModeration", "AppAvailabilityStatus", "AppOperatingSystemType"],
                "DynamicTextAdGroupFieldNames":
                    ["DomainUrl", "DomainUrlProcessingStatus"],
                "DynamicTextFeedAdGroupFieldNames":
                    ["Source", "SourceType", "SourceProcessingStatus"],
                "SmartAdGroupFieldNames":
                    ["FeedId", "AdTitleSource", "AdBodySource"],
            }
        }
        chunk_ad_groups = YandexDirectApi.ad_groups.request(body=payload,
                                                            account_id=campaign.account_login,
                                                            client_login=campaign.client_login,
                                                            entity_name='AdGroups')
        ad_groups += chunk_ad_groups
    # Убираем дубли.
    ad_groups = list({ad_group['Id']: ad_group for ad_group in ad_groups}.values())
    return ad_groups


def get_ads(ad_group_ids):
    """Возвращает список объявлений группы объявлений

    :param ad_group_ids: идентификаторы групп объявлений
    :return: list: список объявлений
    """
    if not len(ad_group_ids):
        return []
    ads = []
    # Для извлечения аккаунта и клиента берем первую группу объявлений.
    ad_group = AdGroup(NameSystemType.YandexDirect, ad_group_ids[0])
    for chunk_ad_groups in chunked(ad_group_ids, 1000):
        fields_for_request = [
            "AdCategories",
            "AgeLabel",
            "AdGroupId",
            "CampaignId",
            "Id",
            "State",
            "Status",
            "StatusClarification",
            "Type",
            "Subtype"
        ]
        payload = {
            "method": "get",
            "params": {
                "SelectionCriteria": {
                    'AdGroupIds': chunk_ad_groups
                },
                "FieldNames": fields_for_request,
                "TextAdFieldNames":
                    ["AdImageHash", "DisplayDomain", "Href", "SitelinkSetId", "Text", "Title", "Title2", "Mobile",
                     "VCardId", "DisplayUrlPath", "AdImageModeration", "SitelinksModeration", "VCardModeration",
                     "AdExtensions", "DisplayUrlPathModeration", "VideoExtension", "TurboPageId", "TurboPageModeration",
                     "BusinessId"],
                "TextAdPriceExtensionFieldNames":
                    ["Price", "OldPrice", "PriceCurrency", "PriceQualifier"],
                "MobileAppAdFieldNames":
                    ["AdImageHash", "AdImageModeration", "Title", "Text", "Features", "Action", "TrackingUrl"],
                "DynamicTextAdFieldNames":
                    ["AdImageHash", "SitelinkSetId", "Text", "VCardId", "AdImageModeration", "SitelinksModeration",
                     "VCardModeration", "AdExtensions"],
                "TextImageAdFieldNames":
                    ["AdImageHash", "Href", "TurboPageId", "TurboPageModeration"],
                "MobileAppImageAdFieldNames":
                    ["AdImageHash", "TrackingUrl"],
                "TextAdBuilderAdFieldNames":
                    ["Creative", "Href", "TurboPageId", "TurboPageModeration"],
                "MobileAppAdBuilderAdFieldNames":
                    ["Creative", "TrackingUrl"],
                "CpcVideoAdBuilderAdFieldNames":
                    ["Creative", "Href", "TurboPageId", "TurboPageModeration"],
                "CpmBannerAdBuilderAdFieldNames":
                    ["Creative", "Href", "TrackingPixels", "TurboPageId", "TurboPageModeration"],
                "CpmVideoAdBuilderAdFieldNames":
                    ["Creative", "Href", "TrackingPixels", "TurboPageId", "TurboPageModeration"],
                "SmartAdBuilderAdFieldNames":
                    ["Creative"]
            }
        }

        chunk_ads = YandexDirectApi.ads.request(body=payload,
                                                account_id=ad_group.account_login,
                                                client_login=ad_group.client_login,
                                                entity_name='Ads')
        ads += chunk_ads
    # Убираем дубли.
    ads = list({ad['Id']: ad for ad in ads}.values())
    return ads

# save methods


def save_clients(account_login):
    """Сохраняет список клиентов агенства. Если аккаунт клиентский, сохраняет его

    :param account_login: логин аккаунта
    """
    clients_source = get_clients(account_login)
    clients = [
        {
            'client_id': client_source.get('Login'),
            'client_name': client_source.get('ClientInfo'),
            'status': client_source.get('Archived'),
            'info': client_source
        }
        for client_source in clients_source
    ]
    Client.create_and_update_many(NameSystemType.YandexDirect, account_login, clients)


def save_campaigns(client_login):
    """Сохраняет список кампаний клиента

    :param client_login: логин клиента
    :return: list: список идентификаторов кампаний
    """
    # Сохраняем кампании
    campaigns_source = get_campaigns(client_login)
    campaigns = [
        {
            'system_id': NameSystemType.YandexDirect.id,
            'client_id': client_login,
            'campaign_id': campaign_source.get('Id'),
            'campaign_name': campaign_source.get('Name'),
            'status': CAMPAIGN_STATUSES.get(campaign_source.get('State'),
                                            campaign_source.get('State')),
            'info': campaign_source
        }
        for campaign_source in campaigns_source
    ]
    campaign_ids = [campaign['campaign_id'] for campaign in campaigns]
    Campaign.create_and_update_many(campaigns)
    return campaign_ids


def save_ad_groups(client_id, campaign_ids):
    """Сохраняет список групп объявлений кампании

    :param campaign_ids: идентификатор кампаний
    :return: list: список идентификаторов групп объявлений
    """
    ad_groups_source = get_ad_groups(campaign_ids)
    ad_groups = [
        {
            'system_id': NameSystemType.YandexDirect.id,
            'client_id': client_id,
            'campaign_id': ad_group_source.get('CampaignId'),
            'ad_group_id': ad_group_source.get('Id'),
            'ad_group_name': ad_group_source.get('Name'),
            'status': None,
            'info': ad_group_source
        }
        for ad_group_source in ad_groups_source
    ]
    ad_group_ids = [ad_group['ad_group_id'] for ad_group in ad_groups]
    AdGroup.create_and_update_many(ad_groups)
    return ad_group_ids


def save_ads(client_id, ad_group_id):
    """Сохраняет список объявлений группы объявлений

    :param client_id:
    :param ad_group_id: идентификатор группы объявлений
    :return: list: список идентификаторов объявлений
    """
    ads_source = get_ads(ad_group_id)
    ads = [
        {
            'system_id': NameSystemType.YandexDirect.id,
            'client_id': client_id,
            'campaign_id': ad_source.get('CampaignId'),
            'ad_group_id': ad_source.get('AdGroupId'),
            'ad_id': ad_source.get('Id'),
            'ad_name': ad_source.get('Name'),
            'status': ad_source.get('State'),
            'info': ad_source
        }
        for ad_source in ads_source
    ]
    ad_ids = [ad['ad_id'] for ad in ads]
    Ad.create_and_update_many(ads)
    return ad_ids


def save_all(client_login):
    """Сохраняет все кампании, группы объявлений, объявления клиента

    :param client_login: логин клиента
    """
    campaign_ids = save_campaigns(client_login)
    ad_group_ids = save_ad_groups(client_login, campaign_ids)
    save_ads(client_login, ad_group_ids)

from urllib import parse

from app.types import CheckResult


def fetch_info_from_campaign_by_types(info, types_campaign: list = None):
    available_types = ['TextCampaign', 'SmartCampaign', 'DynamicTextCampaign', 'MobileAppCampaign', 'CpmBannerCampaign']

    types_campaign = types_campaign if types_campaign else available_types

    for type_campaign in types_campaign:
        if info.get(type_campaign):
            return info.get(type_campaign)

# rules


def campaign_budget(campaign_info: dict) -> CheckResult:
    """Ограничение по бюджету"""
    budget = campaign_info.get('DailyBudget')
    if budget:
        return CheckResult.SUCCESSFUL
    return CheckResult.NOT_SUCCESSFUL


def campaign_strategy(campaign_info: dict) -> CheckResult:
    """Автостратегии"""
    campaign = fetch_info_from_campaign_by_types(campaign_info)

    if campaign is None:
        return CheckResult.SKIPPED

    search_strategy = campaign.get('BiddingStrategy').get('Search', {}).get('BiddingStrategyType')
    network_strategy = campaign.get('BiddingStrategy').get('Network', {}).get('BiddingStrategyType')
    # Если обе площаки выключены, то автостратегии нет.
    if search_strategy == 'SERVING_OFF' and network_strategy == 'SERVING_OFF':
        return CheckResult.NOT_SUCCESSFUL
    # Включена не ручная стратегия.
    search_auto = search_strategy != 'AVERAGE_CPA'
    network_auto = network_strategy != 'HIGHEST_POSITION'
    if network_strategy == 'NETWORK_DEFAULT':
        network_auto = search_auto
    # Обе площадки не ручные.
    if search_auto and network_auto:
        return CheckResult.SUCCESSFUL
    else:
        return CheckResult.NOT_SUCCESSFUL


def ad_utm(ad_info: dict) -> CheckResult:
    """Разметка"""
    final_url = None
    # Добавляем все ссылки объявлений.
    ad = (ad_info.get('TextAd') or ad_info.get('TextImageAd') or ad_info.get('TextAdBuilderAd') or
          ad_info.get('CpcVideoAdBuilderAd') or ad_info.get('CpmBannerAdBuilderAd') or
          ad_info.get('CpmVideoAdBuilderAd'))
    if ad:
        final_url = ad.get('Href', '')
    # Проверяем наличие параметров в ссылке.
    if not final_url:
        return CheckResult.NOT_SUCCESSFUL
    query = parse.parse_qs(parse.urlparse(final_url).query)
    params = set(query.keys())
    if not {'utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term'}.issubset(params):
        return CheckResult.NOT_SUCCESSFUL
    # Все ссылки валидны.
    return CheckResult.SUCCESSFUL

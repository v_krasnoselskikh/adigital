import logging
from datetime import datetime, timedelta
from requests import Request
from base64 import b64encode
from .api import YandexDirectApi
from app.types import NameSystemType, AccountType
from app.controllers import Account, Credentials, Project
from config import YandexDirectApiKey

LOGGER_AUTH = logging.getLogger('Direct.Auth')


def get_app_auth_headers() -> dict:
    """ Получит заголовки API для авторизаци приложения"""
    string_access = f'{YandexDirectApiKey.CLIENT_ID.val}:{YandexDirectApiKey.CLIENT_SECRET.val}'
    string_access = b64encode(bytes(string_access, 'utf-8')).decode("utf-8")
    return {
        'Content-type': 'application/x-www-form-urlencoded',
        'Authorization': f'Basic {string_access}'
    }


def authorize_link():
    """Сформирует ссылку на авторизацию приложения

    :return str: Ссылка на страницу авторизации
    """
    params = dict(
        response_type='code',
        client_id=YandexDirectApiKey.CLIENT_ID.val,
        redirect_uri=YandexDirectApiKey.CALLBACK_URL.val,
        force_confirm='yes'
    )
    url_auth = Request('GET', YandexDirectApi.authorize.url, params=params).prepare().url
    return url_auth


def __get_credentials(code):
    """Обменяет код на ключи авторизации

    :param code: Код, который возвращает Direct на Callback URL
    :return: dict({
          "access_token": "token_string",
          "expires_in": 31535973,
          "refresh_token": "token_string",
          "token_type": "bearer"
        }): ключи авторизации
    """
    try:
        api = YandexDirectApi.token
        api.headers = get_app_auth_headers()

        token = api.request({
            'grant_type': 'authorization_code',
            'code': code
        }, as_post=True)
        return token
    except ConnectionError as e:
        LOGGER_AUTH.warning('Не получился обмен кода на ключ')
        raise e


def get_user_info_by_access_token(access_token):
    """ Получим информацию о владельце токена

    :param access_token: access_token который был получен
    :return: dict({
            'first_name': 'Иван',
            'last_name': 'Иванов',
            'display_name': 'Компания',
            'real_name': 'ИП Иванов',
            'login': 'логин',
            'sex': 'мужской',
            'id': '1'
        }): информация о владельце токена (если запрос неудачный, то False)
    """

    api = YandexDirectApi.info
    api.headers = {
        'Authorization': 'OAuth ' + access_token
    }

    return api.request({})


def get_type_account(account_id) -> AccountType:
    api_settings = YandexDirectApi.clients.request({
        "method": "get",
        "params": {
            "FieldNames": ["Type"]
        }
    }, account_id=account_id, entity_name='Clients')
    type_acc = api_settings[0].get('Type')
    LOGGER_AUTH.info(f'Получен аккаунт {account_id}:{type_acc}')
    return AccountType.Agency if type_acc == 'AGENCY' else AccountType.Client


def by_pass_code_and_save_credentials(code, current_user):
    """ Обменяет код автормзации на ключи, сохранит ключи от аккаунта в базу"""
    credentials = __get_credentials(code)
    credentials['expires_in'] = datetime.utcnow() + timedelta(seconds=credentials['expires_in'])

    user_info = get_user_info_by_access_token(credentials.get('access_token'))

    account_params = dict(
        system=NameSystemType.YandexDirect,
        account_id=user_info['login'],
        account_name=user_info['real_name'],
        info=user_info,
        project_id=Project.get_user_project(current_user.id).get_id()
    )

    account = Account.create_or_update(**account_params)
    account.set_credential(**credentials)
    # Пролучаем тип аккаунта: агентский или клиетский
    type_account = get_type_account(user_info['login'])
    account.update_account_info(account_type=type_account)
    account.save_clients()
    return True


def get_new_access_token(refresh_token):
    """Обменяет refresh_token на новые ключи авторизации

    :param refresh_token: Refresh-токен, полученный от Direct.OAuth вместе с OAuth-токеном.
    :return: dict({
          "access_token": "token_string",
          "expires_in": 31535973,
          "refresh_token": "token_string",
          "token_type": "bearer"
        }): ключи авторизации
    """

    api = YandexDirectApi.token
    api.headers = get_app_auth_headers()
    try:
        response = api.request({
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token
        }, as_post=True)
        response['expires_in'] = datetime.utcnow() + timedelta(seconds=response['expires_in'])
        LOGGER_AUTH.info('Обменял refresh_token на новые ключи авторизации ')
        return response
    except ConnectionError:
        LOGGER_AUTH.error('Не смог обменять refresh_token на новые ключи авторизации ')

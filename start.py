import os
from flask import Flask
from flask_cors import CORS

from app.handlers import register_blueprints
from app.login_manager import login_manager
from app.scheduler import scheduler


# Flask app setup
app = Flask(__name__)
app.secret_key = os.environ.get("SECRET_KEY") or os.urandom(24)
app.url_map.strict_slashes = False
app.config['JSON_AS_ASCII'] = False
CORS(app, supports_credentials=True)

# User session management setup
login_manager.init_app(app)

# Blueprints
register_blueprints(app)

# Scheduler
scheduler.api_enabled = True
scheduler.start()

# Run
if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)

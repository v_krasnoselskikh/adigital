const webpack = require("webpack");
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: {
        "app": path.resolve(__dirname, 'src', 'index.jsx')
    },
    module: {
        rules: [
            {
                "test": /\.jsx?$/,
                "use": {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react'],
                        plugins: [
                            '@babel/plugin-proposal-object-rest-spread',
                            '@babel/plugin-proposal-throw-expressions'
                        ]
                    }
                }
            },
            {
                "test": /\.css$/,
                "use": [
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /\.(png|svg|jpg|gif|ico)$/,
                use: [
                    'file-loader',
                ],
            },
        ]
    },

    resolve: {
        extensions: ['.js', '.jsx']
    },

    optimization: {
        splitChunks: {
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    enforce: true,
                    chunks: 'all'
                }
            }
        }
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'АСУР Adigital',
            filename: 'index.html',
            template: 'src/assets/index.html',
            favicon: 'src/assets/favicon.ico'
        }),
    ],

    output: {
        filename: "[name].[hash].js",
        path: path.resolve(__dirname, 'dist'),
        publicPath: "/static/dist/"
    },

    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        port: 5001,
        host: '0.0.0.0',
        hot: true,
        proxy: {
            '*': {
                target: 'http://api:5000',
                secure: false,
                changeOrigin: true,
                withCredentials: true,
                headers: {host: 'api:9000'}
            }
        }
    }
};

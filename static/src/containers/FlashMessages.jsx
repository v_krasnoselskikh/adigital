import React from 'react'
import {connect} from 'react-redux'
import FlashMessages from "../components/FlashMessages";
import {getFlashedMessages} from '../actions/flash_messages'



const mapStateToProps = (state, ownProps) => {
    const messages = state.messages.list
    return{messages}
}

const mapDispatchToProps = (dispatch, ownProps) => ({
    getFlashedMessages: () => dispatch(getFlashedMessages())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FlashMessages)
import React from 'react'
import {connect} from 'react-redux'
import Accounts  from "../components/Pages/Accounts/Accounts";
import {getAccounts, getAccountClients} from '../actions/accounts'


const mapStateToProps = (state, ownProps) => ({
    list_acc: state.accounts.list,
    error: state.accounts.error
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    getAccounts: () => dispatch(getAccounts()),
    getAccountClients: (account_id) => dispatch(getAccountClients(account_id))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Accounts)
import React from 'react'
import {connect} from 'react-redux'
import Project from "../components/NavBar/Project";
import {getProjects} from '../actions/projects'


const mapStateToProps = (state, ownProps) => ({
    is_login: state.login.is_login,
    ...state.project
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    getProject: dispatch(getProjects())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Project)
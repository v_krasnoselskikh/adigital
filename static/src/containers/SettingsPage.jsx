import React from 'react'
import {connect} from 'react-redux'
import SettingsPage from "../components/Pages/Settings/SettingsPage";
import {getAccounts, getAccountClients} from '../actions/accounts'


const mapStateToProps = (state, ownProps) => ({
    list_acc: state.accounts.list,
    error: state.accounts.error
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    getAccounts: () => dispatch(getAccounts()),
    getAccountClients: (account_id) => dispatch(getAccountClients(account_id))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingsPage)
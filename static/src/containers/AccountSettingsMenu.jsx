import React from 'react'
import {connect} from 'react-redux'
import AccountSettingsMenu  from "../components/Pages/Accounts/AccountSettingsMenu";
import {deleteAccount} from '../actions/accounts'


const mapDispatchToProps = (dispatch, ownProps) => ({
    deleteAccount: () => dispatch(deleteAccount(ownProps.account_id)),
})

export default connect(
    undefined,
    mapDispatchToProps
)(AccountSettingsMenu)
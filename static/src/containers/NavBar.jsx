import React from 'react'
import {connect} from 'react-redux'
import NavBar from "../components/NavBar/NavBar";
import {checkLogin, logout} from '../actions/login'


const mapStateToProps = (state, ownProps) => ({
    ...state.login
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    checkLogin: () => dispatch(checkLogin()),
    logout: () => dispatch(logout())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NavBar)
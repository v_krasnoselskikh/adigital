import React from 'react'
import {connect} from 'react-redux'
import AccountMenu from "../components/NavBar/AccountMenu";
import {checkLogin, logout} from '../actions/login'


const mapStateToProps = (state, ownProps) => ({
    ...state.login
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    checkLogin: () => dispatch(checkLogin()),
    logout: (callback) => dispatch(logout(callback))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AccountMenu)  
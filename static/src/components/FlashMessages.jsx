import React, {useEffect} from "react";
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";


export default function FlashMessages({messages, getFlashedMessages}) {

    useEffect(() => {
        getFlashedMessages()
        setInterval(getFlashedMessages, 15000)
    }, [])

    return <>
        <Container>
            <Box mt={3}>

                <Stack sx={{width: '100%'}} spacing={2}>
                    {messages.map((message) => (
                        <Alert key={message} severity={message.category === 'error' ? 'error' : 'success'}>
                            {message.message}
                        </Alert>
                    ))}
                </Stack>

            </Box>
        </Container>
    </>

}
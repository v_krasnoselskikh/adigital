import React, {useEffect} from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service

  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <Container maxWidth={'lg'}>
        <Typography variant={'h1'}>Что то пошло не так...</Typography>
        <Typography variant={'body1'} mb={3}>Попробуйте вернуться на главную страницу и еще раз выполнить последние действия. </Typography>
        <Button variant={"contained"} color={"primary"} href={'/'}>Вернуться на главную</Button>
      </Container>;
    }

    return this.props.children;
  }
}

export default ErrorBoundary
import React from 'react';
import AssignmentIcon from '@mui/icons-material/Assignment';
import Button from "@mui/material/Button";
import {useNavigate} from "react-router-dom";
import Hidden from "@mui/material/Hidden";

export default function Project({name, is_login}) {
    const navigate = useNavigate()
    if (name === undefined || !is_login) {
        return <></>
    }

    return <>
        <Button
            variant="contained"
            color="primary"
            size="large"
            title={"Проект в сисетме"}
            onClick={() => navigate('/account')}
            startIcon={<AssignmentIcon/>}
        >
            <Hidden mdDown>Проект: {name}</Hidden>
        </Button>
    </>
}

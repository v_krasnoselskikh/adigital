import Badge from "@mui/material/Badge";
import SettingsIcon from "@mui/icons-material/Settings";
import IconButton from "@mui/material/IconButton";
import React from "react";
import {useNavigate} from "react-router-dom";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles((theme) => ({

    settingsButton: {
        marginRight: theme.spacing(2),
        justifySelf: "right"
    },
}));

export default function SettingsButton() {
    const navigate = useNavigate()
    const classes = useStyles();

    return (
        <IconButton onClick={() => navigate('/settings') }
                    className={classes.settingsButton}
                    color="inherit"
                    edge="start"
                    title="Настроки / аккаунты"
        >
            <Badge>
                <SettingsIcon/>
            </Badge>
        </IconButton>
    )
}
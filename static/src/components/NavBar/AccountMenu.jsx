import React from 'react';
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Hidden from "@mui/material/Hidden";
import {useNavigate} from "react-router-dom";



export default function AccountMenu({is_login, user_info, logout}) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const navigate = useNavigate();
    if (is_login !== true) {
        return <></>
    }

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const goToLogin=(e)=>{
        return navigate('/login');
    }


    return <>
        <Button
            variant="contained"
            color="primary"
            size="large"
            onClick={handleClick}
            endIcon={<AccountCircleIcon/>}
            title={'Открыть меню профиля'}
        >
            <Hidden smDown>{user_info.name}</Hidden>
        </Button>

        <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
        >
            <MenuItem onClick={goToLogin}>Выйти</MenuItem>
        </Menu>
    </>


}

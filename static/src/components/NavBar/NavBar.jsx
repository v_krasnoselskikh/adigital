import React from 'react';
import {makeStyles} from '@mui/styles';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import AccountMenu from "../../containers/AccountMenu";
import Project from "../../containers/projects";
import LogoImage from "../../assets/images/logo.svg"
import Box from "@mui/material/Box";
import SettingsButton from "./SettingsButton";
import {useNavigate} from "react-router-dom";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import {ClickAwayListener, Grow, MenuList, Popper} from "@mui/material";
import Paper from "@mui/material/Paper";
import MenuItem from "@mui/material/MenuItem";
import MenuIcon from '@mui/icons-material/Menu';

const useStyles = makeStyles((theme) => ({
    loginButton: {
        marginRight: theme.spacing(2),
    },
    settingsButton: {
        marginRight: theme.spacing(2),
        justifySelf: "right"
    },
    title_logo: {
        cursor: "pointer"
    },
    logo: {
        maxWidth: 100,
        padding: '0px 15px',
        marginTop: 5
    },
    toolbar: {
        justifyContent: "space-between"
    }
}));

export default function NavBar({is_login, user_info, logout}) {
    const classes = useStyles();
    const navigate = useNavigate();
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);

    function handleClickToHome() {
        setOpen(false);
        return navigate("/login");

    }

    function handleClickToTariffs() {
         setOpen(false);
        return navigate("/offer/tariffs");
    }

    function handleClickToDocumentation() {
        setOpen(false);
        return navigate("/documentation");
    }

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        setOpen(false);
    };

    const handleSignIn = ()=>{
        setOpen(false);
        location.href="/google/login";
    }


    return (
        <div className={classes.root}>
            <AppBar position="static" title={<img src={LogoImage} className={classes.logo} alt="Логотип артсофте"/>}>
                <Toolbar className={classes.toolbar}>
                    <Typography
                        className={classes.title_logo}
                        variant="h6"
                        noWrap
                        title={"Логотип"}
                        onClick={handleClickToHome}
                    >
                        <img src={LogoImage} className={classes.logo} alt="Логотип артсофте"/>
                    </Typography>
                    {is_login && <Project/>}
                    {is_login &&
                        <Box>
                            <SettingsButton/>
                            <AccountMenu/>
                        </Box>
                    }
                    {!is_login &&
                        <Box>
                            <Box sx={{display: {'xs': 'none', 'md': 'flex'}}}>
                                <Button color="inherit" onClick={handleClickToDocumentation}
                                        title="Документация">Документация</Button>
                                <Button color="inherit" onClick={handleClickToTariffs} title="Тарифы">Тарифы</Button>
                                <Button color="inherit" onClick={handleSignIn} title="Войти">Войти</Button>
                            </Box>
                            <Box sx={{display: {'xs': 'block', 'md': 'none'}}}>
                                <IconButton
                                    color="inherit"
                                    ref={anchorRef}
                                    id="composition-button"
                                    aria-controls={open ? 'composition-menu' : undefined}
                                    aria-expanded={open ? 'true' : undefined}
                                    aria-haspopup="true"
                                    onClick={handleToggle}
                                >
                                    <MenuIcon/>
                                </IconButton>
                                <Popper
                                    open={open}
                                    anchorEl={anchorRef.current}
                                    role={undefined}
                                    placement="bottom-start"
                                    transition
                                    disablePortal
                                >
                                    {({TransitionProps, placement}) => (
                                        <Grow
                                            {...TransitionProps}
                                            style={{
                                                transformOrigin:
                                                    placement === 'bottom-start' ? 'left top' : 'left bottom',
                                            }}
                                        >
                                            <Paper>
                                                <ClickAwayListener onClickAway={handleClose}>
                                                    <MenuList
                                                        autoFocusItem={open}
                                                        id="composition-menu"
                                                        aria-labelledby="composition-button"
                                                    >
                                                        <MenuItem
                                                            onClick={handleClickToDocumentation}>Документация</MenuItem>
                                                        <MenuItem onClick={handleClickToTariffs}>Тарифы</MenuItem>
                                                        <MenuItem onClick={handleSignIn}>Войти</MenuItem>
                                                    </MenuList>
                                                </ClickAwayListener>
                                            </Paper>
                                        </Grow>
                                    )}
                                </Popper>
                            </Box>
                        </Box>

                    }
                </Toolbar>
            </AppBar>
        </div>
    );
}
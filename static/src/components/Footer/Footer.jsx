import React from "react";
import Box from "@mui/material/Box";
import {grey} from "@mui/material/colors";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import LogoImage from "../../assets/images/logo.svg";
import {useNavigate} from "react-router-dom";
import Button from "@mui/material/Button";
import Link from "@mui/material/Link";

const Footer = () => {
    const navigate = useNavigate();
    const year = new Date().getFullYear();

    function handleClickToHome() {
        return navigate("/login");
    }

    return <Box sx={{
        marginTop: 4,
        background: '#3f4452',
        padding: '10px 0 0'

    }}>
        <Container>
            <Box sx={{display: 'flex', justifyContent: 'space-between', flexDirection: {'xs': 'column', 'md': 'row'}}}>
                <Box>
                    <Typography
                        sx={{cursor: "pointer"}}
                        variant="h6"
                        noWrap
                        title={"Логотип"}
                        onClick={handleClickToHome}
                    >
                        <img src={LogoImage} style={{
                            maxWidth: 100,
                            padding: '0px 15px',
                            marginTop: 5
                        }} alt="Логотип артсофте"/>
                    </Typography>
                    <Box>
                        <Typography color={"white"}>Екатеринбург ул. Малышева 71, 7 этаж, офис 701</Typography>
                        <a href="mailto:info@asur-adigital.ru">
                            <Typography color={"white"}
                                        component={"span"}>info@asur-adigital.ru</Typography></a>

                        <Typography color={"white"}>© 2004-{year} ООО "Адиджитал", ИНН 6685145735</Typography>

                    </Box>
                </Box>

                <Box sx={{
                    marginTop: {'xs': 4, 'md': 0},
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "flex-start",
                    alignItems: "flex-start"
                }}>
                    <Button
                        sx={{color: "#eee"}}
                        onClick={(event) => {
                            event.preventDefault();
                            navigate('/contacts')
                        }} href='/contacts'>Реквизиты</Button>
                    <Button
                        sx={{color: "#eee"}}
                        color="error"
                        onClick={(event) => {
                            event.preventDefault();
                            navigate('/licence')
                        }} href='/licence'>Лицензионная оферта</Button>
                    <Button
                        sx={{color: "#eee"}}
                        onClick={(event) => {
                            event.preventDefault();
                            navigate('/confidential')
                        }} href={'/confidential'}>Политика конфиденциальности</Button>
                     <Button
                        sx={{color: "#eee"}}
                        onClick={(event) => {
                            event.preventDefault();
                            navigate('/personal-data-consent')
                        }} href={'/personal-data-consent'}>Согласие на обработку персональных данных</Button>
                    <Button
                        sx={{color: "#eee"}}
                        onClick={(event) => {
                            event.preventDefault();
                            navigate('/company')
                        }} href={'/confidential'}>ООО "Адиджитал" - информация об ИТ-компании</Button>
                </Box>
            </Box>

            <Box display={'flex'} justifyContent={"center"}>
                <Typography component={'div'} variant={"caption"} mt={2} mb={1}>
                    <Link href={'https://reestr.digital.gov.ru/reestr/309853/3002004'} sx={{color: grey[400]}}>
                        Программа зарегистрирована в Едином реестре российских программ для электронных вычислительных
                        машин
                        и
                        баз данных за номером в реестре №8503 от 30.12.2020
                    </Link>
                </Typography>
            </Box>
        </Container>

    </Box>
}

export default Footer
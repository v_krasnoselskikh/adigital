import React from "react";
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';


export default function AlertError({error}) {
    return (
        <Alert severity="error">
            <AlertTitle>Ошибка</AlertTitle>
            {error.toString()}
        </Alert>
    )

}
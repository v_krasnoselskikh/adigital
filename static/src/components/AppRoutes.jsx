import React, {useEffect} from 'react';
import LoginForm from "./Pages/Login"
import Clients from "./Pages/Clients";
import {BrowserRouter, Navigate, Route, Routes} from 'react-router-dom';
import LinearProgress from "@mui/material/LinearProgress";
import Accounts from "../containers/Accounts";
import SettingsPage from "../containers/SettingsPage";
import Documentation from "./Pages/Documentation";
import Tariffs from "./Pages/Tariffs";
import ErrorBoundary from "./ErrorBoundary";
import Licence from "./Pages/Licence";
import Contacts from "./Pages/Contacts";
import Confidential from "./Pages/Confidential";
import PersonalDataConsent from "./Pages/PersonalDataConsent";
import Campaigns from "../components/Pages/Campaigns/Campaigns";
import PrivateRoute from "./PrivateRoute";
import {Layout} from "./Layout";
import {useDispatch, useSelector} from "react-redux";
import {checkLogin} from "../actions/login";
import ScrollToTop from "./ScrollToTop";
import BasicRules from "./Pages/BasicRules";
import Company from "./Pages/Company";


export default function AppRoutes() {
    const dispatch = useDispatch();
    const is_login = useSelector(state => state.login.is_login)
    useEffect(() => {
        dispatch(checkLogin())
    }, [])

    if (is_login === undefined) {
        return <LinearProgress/>
    }

    return <ErrorBoundary>
        <BrowserRouter>
            <ScrollToTop/>
            <Routes>
                <Route path="/" element={<Layout/>}>
                    <Route path="/" element={is_login ? <Accounts/> : <Navigate to={'/login'} replace/>}/>
                    <Route path="login" element={<LoginForm/>}/>
                    <Route path="offer/tariffs" element={<Tariffs/>}/>
                    <Route path="documentation" element={<Documentation/>}/>
                    <Route path="company" element={<Company/>}/>
                    <Route path={'account'} element={<PrivateRoute/>}>
                        <Route path="/account" element={<Accounts/>}/>
                        <Route path=":account_id" element={<Clients/>}/>
                        <Route path=":account_id/client/:client_id" element={<Campaigns/>}/>
                    </Route>
                    <Route path={'settings'} element={<PrivateRoute/>}>
                        <Route path="/settings" element={<SettingsPage/>}/>
                    </Route>
                    <Route path={'licence'} element={<Licence/>}/>
                    <Route path={'contacts'} element={<Contacts/>}/>
                    <Route path={'confidential'} element={<Confidential/>}/>
                    <Route path={'personal-data-consent'} element={<PersonalDataConsent/>}/>
                    <Route path={'basic-rules'} element={<BasicRules/>}/>
                    <Route path="*" element={<p>Страница не найдена: 404!</p>}/>
                </Route>
            </Routes>
        </BrowserRouter>
    </ErrorBoundary>
}


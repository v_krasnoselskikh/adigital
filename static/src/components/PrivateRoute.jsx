import React from "react";

import {useSelector} from "react-redux";
import {Navigate, useLocation, Outlet} from "react-router-dom";

export default function PrivateRoute({children}) {
    const location = useLocation();
    const {is_login: authUser} = useSelector(state => state.login);

    if (!authUser) {
        // not logged in so redirect to login page with the return url
        return <Navigate to="/login" state={{from: location}}/>
    }

    // authorized so return child components
    return <Outlet/>;
}


import React, {useReducer, useState} from "react";
import Typography from "@mui/material/Typography";
import {
    Checkbox,
    FormControl,
    FormControlLabel,
    FormGroup,
    Input,
    InputAdornment,
    InputLabel,
    TextField
} from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import LinearProgress from "@mui/material/LinearProgress";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";

import {IMaskInput} from 'react-imask';


const formReducer = (state, event) => {
    return {
        ...state,
        [event.name]: event.value
    }
}

const TextMaskCustom = React.forwardRef(function TextMaskCustom(props, ref) {
    const {onChange, ...other} = props;
    return (
        <IMaskInput
            {...other}
            mask="# (#00) 000-0000"
            definitions={{
                '#': /[1-9]/,
            }}
            inputRef={ref}
            onAccept={(value) => onChange({target: {name: props.name, value}})}
            overwrite
        />
    );
});

const FeedbackForm = () => {

    const [submitting, setSubmitting] = useState(false);
    const [error, setError] = useState();
    const [isSend, setIsSend] = useState(false);

    const [formData, setFormData] = useReducer(formReducer, {});


    const send_request = async () => {
        return await fetch('/email/request-for-connection', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        })
    }

    const handleSubmit = event => {
        event.preventDefault();

        setSubmitting(true);
        setError(undefined);

        send_request()
            .then(r => {
                    if (r.status === 200) {
                        setSubmitting(false)
                        setIsSend(true)
                    } else {
                        setError(r.text())
                    }
                }
            )
            .catch(e => {
                setError(e)
            })
            .finally(() => {
                setSubmitting(false)
            })
    }

    const handleChange = event => {
        setFormData({
            name: event.target.name,
            value: event.target.value,
        });
    }


    return <>

        <Typography variant={'h5'}>Оставьте заявку на подключение</Typography>

        {!isSend &&
            <Typography variant={'body1'} mt={2}>Мы свяжемся с вами в течение 15 минут</Typography>
        }

        <Box mt={3}>
            {submitting && <LinearProgress/>}
            {error && <>
                <Alert severity="error">
                    <AlertTitle>Ошибка</AlertTitle>
                    При отправке сообщения возникла ошибка — <strong>проверьте ваши данные и попробуйте отправить заявку
                    еще раз.</strong>
                </Alert>
            </>}
            {isSend && <>
                <Alert severity="success">
                    <AlertTitle>Заявка отправлена</AlertTitle>
                    Мы свяжемся с вами в течение 15 минут
                </Alert>
            </>}

            {!isSend &&
                <form onSubmit={handleSubmit}>
                    <FormControl fullWidth variant="standard">
                        <InputLabel htmlFor="name">Ф.И.O</InputLabel>
                        <Input
                            id="name"
                            name="name"
                            value={formData.name || ""}
                            onChange={handleChange}
                            required
                        />
                    </FormControl>
                    <FormControl fullWidth variant="standard">
                        <InputLabel htmlFor="phone">Телефон</InputLabel>
                        <Input
                            id="phone"
                            name="phone"
                            value={formData.phone || ""}
                            onChange={handleChange}
                            inputComponent={TextMaskCustom}
                            required
                        />

                    </FormControl>
                    <FormControl fullWidth variant="standard">
                        <InputLabel htmlFor="email">Email</InputLabel>
                        <Input
                            id="email"
                            name="email"
                            value={formData.email || ""}
                            onChange={handleChange}
                            required
                        />
                    </FormControl>

                    <FormGroup>
                        <FormControlLabel
                            control={<Checkbox defaultChecked required/>}

                            label={<Typography variant={'caption'}> Даю <a href={'/personal-data-consent'}>согласие на обработку своих персональных данных</a> и согласен с <a href={'confidential'}>политикой обработки персональных данных</a></Typography>}/>
                    </FormGroup>

                    <Box display={'flex'} mt={2}>
                        <Button variant={"contained"} type="submit">Отправить заявку</Button>
                    </Box>

                </form>
            }
        </Box>
    </>
}

export default FeedbackForm
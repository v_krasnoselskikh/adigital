import Box from "@mui/material/Box";
import Nav_bar from "../containers/NavBar";
import Grid from "@mui/material/Grid";
import FlashMessages from "../containers/FlashMessages";
import Container from "@mui/material/Container";
import {Outlet} from "react-router-dom";
import Footer from "./Footer/Footer";
import React from "react";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    control: {
        padding: theme.spacing(2),
    },
}));

export function Layout() {
    const classes = useStyles();

    return <>
        <Box display={'flex'} flexDirection={'column'} sx={{minHeight: '100vh'}}>

            <Nav_bar/>
            <Box flex={1}>
                <Grid container>
                    <Grid item xs={12}>
                        <FlashMessages/>
                    </Grid>
                    <Grid item xs={12}>
                        <Container maxWidth={'lg'} className={classes.control}>
                            <Outlet/>
                        </Container>
                    </Grid>
                </Grid>
            </Box>
            <Box>
                <Footer/>
            </Box>
        </Box>
    </>
}


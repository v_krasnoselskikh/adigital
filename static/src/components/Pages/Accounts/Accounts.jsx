import React, {useEffect} from "react";
import {makeStyles} from "@mui/styles";
import Paper from "@mui/material/Paper";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import TablePagination from "@mui/material/TablePagination";
import LinearProgress from "@mui/material/LinearProgress";
import AlertError from "../../AlertError";
import { useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import AccountSettingsMenu from "../../../containers/AccountSettingsMenu";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import {Helmet} from "react-helmet";

const columns = [
    // account_id
// system
// account_name
// account_type
// created
    {id: 'system', label: 'Система', minWidth: 170},
    {id: 'account_id', label: 'Ид Аккаунта', minWidth: 100},
    {id: 'account_name', label: 'Аккаунт', minWidth: 100},
    {id: 'account_type', label: 'Тип аккаунта'},
    {id: 'created', label: 'Дата подключения'},
    {id: 'settings'}
];

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: '90vh',
    },
    row: {
        cursor: "pointer"
    }
});

export default function Accounts({list_acc, error, getAccounts, getAccountClients}) {
    const classes = useStyles();
    const navigate = useNavigate();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    useEffect(() => {
        if (list_acc === undefined && error === undefined) {
            getAccounts()
        }
    })

    if (error !== undefined) {
        return <AlertError error={error}/>
    }


    if (list_acc === undefined) {
        return <LinearProgress/>
    }

    const go_to_account = (account_id) => {
        return navigate('/account/' + account_id);
    }

    const go_to_settings = (e) => {
        return navigate('/settings')
    }

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return <>
        <Helmet>
            <title>АСУР Adigital</title>
        </Helmet>
        <Typography variant="h4" component="h2">Подключенные аккаунты</Typography>
        <Typography variant="caption" component="p" paragraph={true}>Выберите из списка аккаунт для
            конфигурации</Typography>
        {list_acc.length === 0 && (
            <Box mt={5}>
                <Paper>
                    <Box p={3}>
                        <Typography variant="h5" gutterBottom>У вас еще не добавлены источники данных.</Typography>
                        <Typography variant="caption" gutterBottom>Перейдите в раздел настроек, чтобы добавить
                            их</Typography>
                        <Box mt={3}>
                            <Button title="Прейти в раздел настроек"
                                    color="primary"
                                    variant="contained"
                                    onClick={go_to_settings}
                            >Прейти в раздел настроек</Button>
                        </Box>
                    </Box>
                </Paper>
            </Box>
        )}
        {list_acc.length > 0 && (
            <Paper>
                <TableContainer className={classes.container}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                {columns.map((column) => (
                                    <TableCell
                                        key={column.id}
                                        align={column.align}
                                        style={{minWidth: column.minWidth}}
                                    >
                                        {column.label}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {list_acc
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((acc) => {
                                    return (
                                        <TableRow
                                            hover
                                            role="checkbox"
                                            className={classes.row}
                                            tabIndex={-1}
                                            key={acc.account_id}
                                        >
                                            {columns.map((column) => {
                                                if (column.id === 'settings') {
                                                    return (<TableCell
                                                        key={column.id}
                                                        align={column.align}>
                                                        <AccountSettingsMenu account_id={acc.account_id}/>
                                                    </TableCell>)
                                                } else {
                                                    return (
                                                        <TableCell
                                                            onClick={() => go_to_account(acc.account_id)}
                                                            key={column.id}
                                                            align={column.align}>
                                                            {acc[column.id]}
                                                        </TableCell>
                                                    );
                                                }
                                            })}
                                        </TableRow>
                                    );
                                })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={list_acc.length}
                    rowsPerPage={rowsPerPage}
                    labelRowsPerPage='Клиентов на странице'
                    page={page}
                    onPageChange={(event, page)=>handleChangePage(event,page)}
                    onRowsPerPageChange={e=>handleChangeRowsPerPage(e)}
                />
            </Paper>
        )}

    </>
}
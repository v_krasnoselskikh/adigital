import React from "react";
import IconButton from "@mui/material/IconButton";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import SettingsIcon from '@mui/icons-material/Settings';

export default function AccountSettingsMenu({account_id, deleteAccount}) {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleDelete = () => {
        deleteAccount();
        setAnchorEl(null);
    };

    return (
        <div>
            <IconButton aria-label="Настройки системы" component="span"
                        aria-controls={`settings_menu_acc_${account_id}`}
                        aria-haspopup="true"
                        onClick={handleClick}>
                <SettingsIcon/>
            </IconButton>
            <Menu
                id={`settings_menu_acc_${account_id}`}
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={handleDelete}>Удалить подключение</MenuItem>
            </Menu>
        </div>
    );

}
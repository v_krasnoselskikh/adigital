import React, {useEffect} from "react";
import {makeStyles} from "@mui/styles";
import Paper from "@mui/material/Paper";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import TablePagination from "@mui/material/TablePagination";
import LinearProgress from "@mui/material/LinearProgress";
import AlertError from "../AlertError";
import Typography from "@mui/material/Typography";
import CircularProgress from "@mui/material/CircularProgress";
import {useNavigate, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getAccountClients} from "../../actions/accounts";

const columns = [
// client_id
// client_name
// system
// status
// created

    {id: 'client_id', label: 'Ид Клиента', minWidth: 100},
    {id: 'client_name', label: 'Название аккаунта клиента'},
    {id: 'created', label: 'Дата подключения'}
];

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: '90vh',
    },
    row: {
        cursor: "pointer"
    }
});

export default function Clients() {
    const classes = useStyles();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const {account_id} = useParams();


    const list_clients = useSelector(state => state.clients.list)
    const error = useSelector(state => state.clients.error)
    const is_loaded = useSelector(state => state.clients.is_loaded)


    useEffect(() => {
        dispatch(getAccountClients(account_id))
    }, [])

    if (error !== undefined) {
        return <AlertError error={error}/>
    }


    if (list_clients === undefined) {
        return <LinearProgress/>
    }

    const go_to_campaigns = (client_id) => {
        return navigate(`/account/${account_id}/client/${client_id}`);
    }


    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const list_clients_filtered = list_clients
        .filter(e => e.account === account_id)


    return <>

        <Typography variant="h4" component="h2" gutterBottom>
            Рекламные кабинеты в аккаунте
        </Typography>
        <Typography variant="body1" gutterBottom>Аккаунт: {account_id}</Typography>
        <Typography variant="caption" component="p" paragraph={true}>Выберите из списка аккаунт клиента для
            конфигурации</Typography>
        <Paper className={classes.root}>
            {!is_loaded && <CircularProgress/>}
            {is_loaded &&
                <>
                    <TableContainer className={classes.container}>
                        <Table stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                    {columns.map((column) => (
                                        <TableCell
                                            key={column.id}
                                            align={column.align}
                                            style={{minWidth: column.minWidth}}
                                        >
                                            {column.label}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {list_clients_filtered
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((acc) => {
                                        return (
                                            <TableRow
                                                hover
                                                role="checkbox"
                                                className={classes.row}
                                                onClick={() => go_to_campaigns(acc.client_id)}
                                                tabIndex={-1}
                                                key={acc.client_id}>
                                                {columns.map((column) => {
                                                    const value = acc[column.id];
                                                    return (
                                                        <TableCell key={column.id} align={column.align}>
                                                            {value}
                                                        </TableCell>
                                                    );
                                                })}
                                            </TableRow>
                                        );
                                    })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={list_clients_filtered.length}
                        rowsPerPage={rowsPerPage}
                        labelRowsPerPage='Клиентов на странице'
                        page={page}
                        onPageChange={(event, page) => handleChangePage(event, page)}
                        onChangeRowsPerPage={e => handleChangeRowsPerPage(e)}
                    />
                </>
            }
        </Paper>

    </>
}
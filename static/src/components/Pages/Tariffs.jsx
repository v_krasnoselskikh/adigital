import Typography from "@mui/material/Typography";
import React from "react";
import Box from "@mui/material/Box";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Paper from "@mui/material/Paper";
import TableBody from "@mui/material/TableBody";
import {Helmet} from "react-helmet";


export default function Tariffs() {
    return (
        <Box mt={7}>
            <Helmet>
                <title>Тарифы АСУР Adigital</title>
            </Helmet>
            <Typography variant="h1" component="h1" align="center">Тарифы АСУР Adigital</Typography>

            <Box mt={5}>
                <Typography gutterBottom variant="h4">ООО «Адиджитал»</Typography>
                <Typography gutterBottom>Российская Федерация, Свердловская область, город Екатеринбург</Typography>
                <Typography gutterBottom>Дата размещения: «11» августа 2022 г.</Typography>
                <Typography gutterBottom>Дата вступления в силу: «11» августа 2022 г.</Typography>
            </Box>

            <Box mt={5}>
                <TableContainer component={Paper}>
                    <Table aria-label="Таблица услуг">
                        <TableHead>
                            <TableRow>
                                <TableCell>Услуга</TableCell>
                                <TableCell align="center">Единица измерения</TableCell>
                                <TableCell align="center">Период оказания услуги</TableCell>
                                <TableCell align="center">Стоимость, рублей*</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell>Простая (неисключительная) лицензия на использование<br/> модуля программы для ЭВМ
                                    АСУР Adigital</TableCell>
                                <TableCell align="center">1 штука</TableCell>
                                <TableCell align="center">ежемесячно</TableCell>
                                <TableCell align="center">от 10 000</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
            <Box mt={5}>
                <Typography gutterBottom>* - НДС не облагается</Typography>
            </Box>
        </Box>
    )
}
import Typography from "@mui/material/Typography";
import React from "react";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";

const systems = [
    {
        name: "Google ADS",
        link_auth: "/api/adwords/",
        description: "Будут подключены все кабинеты на аккаунте Google"
    },
    {
        name: "Яндекс.Директ",
        link_auth: "/direct/",
        description: "Можно подключать как агентский так и клиентские кабинеты."
    }
]


export default function SettingsPage() {


    return <>
        <Typography variant="h4" component="h2" gutterBottom>Настройки</Typography>
        <Paper elevation={3}>
            <Box p={2}>
                <Typography variant="h5" gutterBottom>Добавить интеграции к системам</Typography>

                <Typography variant="caption" gutterBottom>Для того, чтобы данные попали в стисему, необходимо
                    подключть коннекторы.<br/> При подключении к рекламным системам вы должны быть авторизованы в этих
                    кабинетах
                    и иметь доступ на чтение данных.
                </Typography>

                <Box pt={2}>
                    <Grid container spacing={3}>
                        {systems.map((e) => (
                            <Grid item xs={6} key={e.name}>
                                <Card>
                                    <CardContent>
                                        <Typography color="textSecondary" gutterBottom>Коннектор к системе </Typography>
                                        <Typography variant="h5" component="h2" gutterBottom>
                                            {e.name}
                                        </Typography>
                                        <Typography gutterBottom>{e.description}</Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button href={e.link_auth} variant="contained"
                                                color="primary">Подключить</Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </Box>

            </Box>
        </Paper>

    </>
}
import React from "react";
import {Helmet} from "react-helmet";
import {Table, TableHead, Box, Typography, Paper, TableContainer, TableCell, TableRow} from "@mui/material";
import TableBody from "@mui/material/TableBody";

const PersonalDataConsent = () => {
    return <>
        <Helmet>
            <title>Согласие на обработку персональных данных</title>
        </Helmet>
        <Box>

            <Typography variant='h3' mt={9} mb={3} fontWeight={"bold"} align={'center'}>Согласие на обработку
                персональных данных</Typography>
            <Typography variant='body2' align={'center'}>(далее - Согласие)</Typography>
            <Typography variant='body2' align={'right'}>11 июня 2024 г.</Typography>
        </Box>

        <Box className="content">
            <p>Присоединяясь к настоящему Согласию и оставляя свои данные на Сайте asur-adigital.ru, (далее – Сайт),
                путем заполнения полей формы (регистрации), используя корпоративную электронную почту
                info@artsofte.digital, Пользователь действует свободно, своей волей и в своем интересе, безусловно
                выражая свое согласие, владельцу сайта asur-adigital.ru – ООО "Адиджитал", ИНН 6685145735 (в дальнейшем
                - Оператор), осуществляющему свою деятельность по адресу: Россия, 620075, Свердловская область, г.
                Екатеринбург, ул. Малышева, д. 71, оф. 701, на обработку (любое действие (операцию) или совокупность
                действий (операций), совершаемых с использованием средств автоматизации (автоматизированная обработка),
                или без использования таких средств (неавтоматизированная обработка)) персональных данных в составе
                следующих категорий (применительно к каждой из обозначенных целей):</p>
            <p>1. Цели обработки персональных данных и перечень персональных данных, в отношении которых Пользователь,
                являющийся субъектом персональных данных, предоставляет согласие на обработку персональных данных (далее
                - Согласие):</p>
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableCell align={"center"} width={1}>Цели обработки персональных данных</TableCell>
                        <TableCell align={"center"} width={1}>Категории персональных данных, обрабатываемые в рамках
                            цели</TableCell>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <p>Идентификация Пользователя, для оформления заказа и (или) заключения договора</p>
                            </TableCell>
                            <TableCell>
                                <ul>
                                    <li>фамилия, имя, отчество Пользователя (в т.ч. по отдельности и в случае
                                        изменения);
                                    </li>
                                    <li>контактный телефон Пользователя;</li>
                                    <li>адрес электронной почты (e-mail) Пользователя;</li>
                                    <li>должность, место работы Пользователя.</li>
                                </ul>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <p>Установление с Пользователем обратной связи, включая направление уведомлений,
                                    запросов, касающихся использования Сайта, оказания услуг, предоставления продуктов в
                                    пользование, обработка запросов и заявок от Пользователя</p>
                            </TableCell>
                            <TableCell>
                                <ul>
                                    <li>фамилия, имя, отчество Пользователя (в т.ч. по отдельности и в случае
                                        изменения);
                                    </li>
                                    <li>контактный телефон Пользователя;</li>
                                    <li>адрес электронной почты (e-mail) Пользователя;</li>
                                    <li>должность, место работы Пользователя.</li>
                                </ul>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <p>Регистрация Пользователя на мероприятия Оператора и его партнеров (вебинары,
                                    конференции, семинары, встречи и т.д.)</p>
                            </TableCell>
                            <TableCell>
                                <ul>
                                    <li>фамилия, имя, отчество Пользователя (в т.ч. по отдельности и в случае
                                        изменения);
                                    </li>
                                    <li>контактный телефон Пользователя;</li>
                                    <li>адрес электронной почты (e-mail) Пользователя;</li>
                                    <li>должность, место работы Пользователя.</li>
                                </ul>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <p>Предоставление Пользователю эффективной клиентской и технической поддержки при
                                    возникновении проблем, связанных с использованием сайта</p>
                            </TableCell>
                            <TableCell>
                                <ul>
                                    <li>фамилия, имя, отчество Пользователя (в т.ч. по отдельности и в случае
                                        изменения);
                                    </li>
                                    <li>контактный телефон Пользователя;</li>
                                    <li>адрес электронной почты (e-mail) Пользователя;</li>
                                </ul>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <p>Предоставление Пользователю информации об обновлениях продукции Оператора,
                                    специальных предложений, информации о ценах, новостной, рекламной рассылки и иных
                                    сведений Оператора или партнеров Оператора;</p>
                            </TableCell>
                            <TableCell>
                                <ul>
                                    <li>фамилия, имя, отчество Пользователя (в т.ч. по отдельности и в случае
                                        изменения);
                                    </li>
                                    <li>контактный телефон Пользователя;</li>
                                    <li>адрес электронной почты (e-mail) Пользователя;</li>
                                    <li>должность, место работы Пользователя.</li>
                                </ul>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <p>Предоставление доступа Пользователю на сайты или сервисы партнеров с целью получения
                                    продуктов, обновлений и услуг</p>
                            </TableCell>
                            <TableCell>
                                <ul>
                                    <li>фамилия, имя, отчество Пользователя (в т.ч. по отдельности и в случае
                                        изменения);
                                    </li>
                                    <li>контактный телефон Пользователя;</li>
                                    <li>адрес электронной почты (e-mail) Пользователя;</li>
                                    <li>должность, место работы Пользователя.</li>
                                </ul>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <p>Рассмотрение кандидатуры Пользователя для замещения вакансии Оператора, его
                                    дальнейшего трудоустройства, а также в целях ведения кадрового резерва</p>
                            </TableCell>
                            <TableCell>
                                <ul>
                                    <li>фамилия, имя, отчество Пользователя (в т.ч. по отдельности и в случае
                                        изменения);
                                    </li>
                                    <li>контактный телефон Пользователя;</li>
                                    <li>адрес электронной почты (e-mail) Пользователя;</li>
                                    <li>Информация об образовании</li>
                                    <li>Информация о месте работы (текущем и прежнем).</li>
                                </ul>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <p>Обеспечение технической возможности использовать Сайт, изучать информацию на нем,
                                    пользоваться его функциональностью,<br/>повышение эффективности использования
                                    Пользователями Сайта</p>
                            </TableCell>
                            <TableCell>
                                <ul>
                                    <li>IP-адрес;</li>
                                    <li>cookies</li>
                                </ul>
                            </TableCell>
                        </TableRow>

                    </TableBody>
                </Table>
            </TableContainer>

            <Typography mt={2}>2. Оператор вправе осуществлять обработку персональных данных следующими способами:
                <ul>
                    <li>сбор;</li>
                    <li>систематизация;</li>
                    <li>накопление;</li>
                    <li>хранение;</li>
                    <li>уточнение (обновление, изменение);</li>
                    <li>использование;</li>
                    <li>передача данных третьим лицам;</li>
                    <li>обезличивание;</li>
                    <li>блокирование;</li>
                    <li>уничтожение;</li>
                </ul>
                а также осуществление любых иных действий с персональными данными в соответствии с действующим
                законодательством Российской Федерации.
            </Typography>
            <Typography>
                3. Пользователь подтверждает, что Оператор вправе в необходимом объеме раскрывать и передавать
                персональные данные следующим третьим лицам:
                <ul>
                    <li>ООО «ЮНИСЕНДЕР СМАРТ», ОГРН: 1227700213180, ИНН: 9731091240, в целях организации направления
                        информационных рассылок о мероприятиях Оператора (по телефону и/или предоставленной электронной
                        почте);
                    </li>
                    <li>ООО «Колтач Солюшнс», ОГРН: 1186658013013, ИНН: 7703388936, в целях предоставления
                        функциональности на программный продукт ООО «Колтач Солюшнс» - программу для ЭВМ
                        «Calltouch-коллтрекинг сервис».
                    </li>
                    <li>ООО «Таргет Телеком», ОГРН: 5147746239427; ИНН: 7743943569 (оператор связи), в целях направления
                        Пользователю уведомлений, обозначенных в настоящей Политике посредством СМС-сообщений; оператор
                        связи обязуется соблюдать требования к безопасности персональных данных на уровне, не ниже
                        предусмотренного Политикой;
                    </li>
                    <li>ООО «ЯНДЕКС», ОГРН: 1027700229193, ИНН: 7736207543, (правообладателю сервиса веб-аналитики
                        Яндекс Метрика), в целях повышения эффективности использования Пользователями Сайта.
                    </li>
                    <li>ООО «В Контакте», ОГРН: 1079847035179, ИНН: 7842349892, в целях повышения эффективности
                        использования Пользователями Сайта;
                    </li>
                    <li>ООО «ВК», ОГРН: 1027739850962, ИНН: 7743001840, в целях повышения эффективности использования
                        Пользователями Сайта.
                    </li>
                    <li>ООО «Управляющая компания «Артсофте», ОГРН: 1176658087132, ИНН: 6685139611 в целях рассмотрение
                        кандидатуры Пользователя для замещения вакансии Оператора, его дальнейшего трудоустройства, а
                        также в целях ведения кадрового резерва.
                    </li>
                </ul>
            </Typography>

            <Typography mt={2}>
                4. Пользователь гарантирует, что:
                <ol type={'a'}>
                    <li>все указанные им данные принадлежат лично ему;</li>
                    <li>он ознакомлен с положениями Федерального закона от 27.07.2006 г. № 152-ФЗ «О персональных
                        данных», права и обязанности в области защиты персональных данных ему известны;
                    </li>
                    <li>им внимательно и в полном объеме прочитана политика Оператора в отношении обработки персональных
                        данных на Сайте, размещенная по адресу: <a
                            href="https://asur-adigital.ru/confidential">https://asur-adigital.ru/confidential</a> Содержание
                        политики
                        полностью понятно Пользователю.
                    </li>
                </ol>
            </Typography>

            <Typography mt={2}>5. Настоящее согласие действует c момента его предоставления Пользователем до момента
                достижения целей обработки персональных данных или до момента отзыва Согласия. Пользователь вправе
                отозвать согласие в любое время путем подачи Оператору заявления с указанием данных, определенных ст. 14
                Закона «О персональных данных» на адрес электронной почты (E-mail) info@artsofte.digital или по адресу
                620075, г. Екатеринбург, а/я 75.</Typography>
            <Typography mt={2}>6. В случае отзыва Пользователем Согласия на обработку персональных данных Оператор
                обязуется прекратить их обработку или обеспечить прекращение такой обработки (если обработка
                персональных данных осуществляется другим лицом, действующим по поручению Оператора) и, в случае если
                сохранение персональных данных более не требуется для целей обработки персональных данных, уничтожить
                персональные данные или обеспечить их уничтожение (если обработка персональных данных осуществляется
                другим лицом, действующим по поручению Оператора) в срок, не превышающий тридцати дней с даты
                поступления отзыва.</Typography>
            <Typography mt={2}>7. Пользователь уведомлен о том, что Оператор вправе продолжить обработку персональных
                данных без его согласия в случаях, предусмотренных законодательством Российской Федерации.</Typography>
            <Typography mt={2}>8. Оператор вправе в любое время в одностороннем порядке вносить изменения в настоящее
                Согласие, путем публикации нового текста Согласия на своем сайте. При внесении изменений в актуальной
                редакции указывается дата последнего обновления. Новая редакция Согласия вступает в силу с момента ее
                размещения, если иное не предусмотрено новой редакцией Согласия. Действующая редакция Согласия находится
                на странице Сайта по адресу: <a href="https://asur-adigital.ru/personal-data-consent">https://asur-adigital.ru/personal-data-consent</a>.</Typography>
            <Typography mt={2}>9. Настоящее Согласие Пользователя признается исполненным в простой письменной
                форме.</Typography>
            <Typography mt={2}>10. К настоящему Согласию и отношениям между Пользователем и Оператором, возникающим в связи
                с применением Согласия, подлежит применению право Российской Федерации.</Typography>

        </Box>
    </>
}

export default PersonalDataConsent
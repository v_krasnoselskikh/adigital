import React, {useEffect} from 'react';
import {makeStyles} from '@mui/styles';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import {Helmet} from "react-helmet";
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../actions/login";
import Paper from "@mui/material/Paper";
import FeedbackForm from "../Forms/FeedbackForm";


const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: 275,
    }
}));

export default function LoginForm() {
    const classes = useStyles();
    const is_login = useSelector(state=>state.login.is_login)
    const dispatch = useDispatch()

    useEffect(()=>{
        if (is_login){
            dispatch(logout())
        }
    }, [is_login])



    return (
        <Box
            fontFamily="h6.fontFamily"
            fontSize={{xs: 'h6.fontSize', sm: 'h4.fontSize', md: 'h3.fontSize'}}
            p={{xs: 2, sm: 3, md: 4}}
            mt={5}
        >
            <Helmet>
                <title>Авторизация в АСУР Adigital</title>
            </Helmet>
            <Container maxWidth="sm">
                <Card className={classes.root}>
                    <CardContent>
                        <Typography variant="h5" component="h2">
                            Авторизуйтесь в системе АСУР
                        </Typography>
                        <Typography variant="subtitle1">
                            с помощью Google Аккаунта
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button variant="contained" color="primary" href="/google/login">Войти с помощью Google</Button>
                    </CardActions>
                </Card>

                <Box mt={9}>
                    <Paper>
                        <Box p={2}>
                            <FeedbackForm/>
                        </Box>
                    </Paper>
                </Box>
            </Container>
        </Box>
    );
}
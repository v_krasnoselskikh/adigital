import Typography from "@mui/material/Typography";
import React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import {Helmet} from "react-helmet";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles((theme) => ({
    image: {
        width: "100%"
    },
    text_offset: {
        paddingTop: "10px"
    }
}));


export default function Company() {
    const classes = useStyles();

    return (
        <Box mt={5}>
            <Helmet>
                <title>О компании</title>
            </Helmet>
            <Typography variant="h3" component="h3" align="left">ООО Адиджитал</Typography>

            <Box mt={5}>
                <Typography mt={2} variant="body1">
                    ООО «Адиджитал» (ИНН: 6670348115) является аккредитованной ИТ-компанией (запись в реестре № 4540 от
                    «21» ноября 2014г.).
                </Typography>
                <Typography mt={2} variant="body1">
                    Основным видом деятельности ООО «Адиджитал» является 63.11 Деятельность по обработке данных,
                    предоставление услуг по размещению информации и связанная с этим деятельность.
                </Typography>

                <Typography mt={2} variant="body1">
                    Организация осуществляет разработку собственного программного обеспечения, коммерциализирует его по
                    лицензионным договорам в адрес третьих лиц. Основным объектом коммерциализации является программа
                    для ЭВМ «Автоматизированная система управления рекламой Artsofte Digital» (АСУР ADigital).
                </Typography>

                <Typography mt={2} variant="body1">
                    Программа для ЭВМ «Автоматизированная система управления рекламой Artsofte Digital» (АСУР ADigital)
                    позволяет осуществлять сбор, хранение, обработку, анализ, визуализацию данных с рекламных систем.
                    Основное назначение: отслеживание ключевых параметров рекламных кампаний, а также управление
                    статусом (остановкой) этих рекламных кампаний.
                </Typography>

                <Typography mt={2} variant="body1">
                    Программа для ЭВМ «Автоматизированная система управления рекламой Artsofte Digital» (АСУР ADigital)
                    включена в Единый российский реестр российского программного обеспечения (запись в реестре от № 8503
                    от 30.12.2020).
                </Typography>
            </Box>

        </Box>
    )
}
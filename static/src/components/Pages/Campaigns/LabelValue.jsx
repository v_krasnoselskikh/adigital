import React from "react";
import Typography from "@mui/material/Typography";
import {makeStyles} from "@mui/styles";
import red from "@mui/material/colors/red";
import green from "@mui/material/colors/green";

const useStyles = makeStyles({
    on: {
        color: green[500],
    },
    off: {
        color: red[500],
    }
});

export default function LabelValue({label_value, on_label, off_label}) {
    const classes = useStyles()
    switch (label_value) {
        case true:
            return <Typography variant="caption" className={classes.on}>{on_label}</Typography>

        case false:
            return <Typography variant="caption" className={classes.off}>{off_label}</Typography>

        case undefined:
            return <div> -- </div>

        case null:
            return <div> -- </div>

        case 'archived':
            return <Typography variant="caption">Архивная</Typography>
        case 'converted':
            return <Typography variant="caption" className={classes.off}>CONVERTED</Typography>
        case 'ended':
            return <Typography variant="caption">Окончена</Typography>
        case 'off':
            return <Typography variant="caption" className={classes.off}>Выключена</Typography>
        case 'on':
            return <Typography variant="caption" className={classes.on}>Активна</Typography>
        case 'suspended':
            return <Typography variant="caption" className={classes.off}>Приостановлена</Typography>
        case 'paused':
            return <Typography variant="caption" className={classes.off}>Приостановлена</Typography>
        case 'enabled':
            return <Typography variant="caption" className={classes.on}>Активна</Typography>

        default:
            return <span>{label_value}</span>
    }


}
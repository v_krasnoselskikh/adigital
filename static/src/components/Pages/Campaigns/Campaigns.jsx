import React, {useEffect} from "react";
import {makeStyles} from "@mui/styles";
import Paper from "@mui/material/Paper";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import TablePagination from "@mui/material/TablePagination";
import LinearProgress from "@mui/material/LinearProgress";
import AlertError from "../../AlertError";
import Typography from "@mui/material/Typography";
import CircularProgress from "@mui/material/CircularProgress";
import LabelValue from "./LabelValue";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import {Link, useParams} from "react-router-dom"
import {useDispatch, useSelector} from "react-redux";
import {getCampaigns} from "../../../actions/campaigns";

const columns = [
// campaign_id
// campaign_name
// system
// status
// has_budget
// has_strategy
// has_utm
// last_updated
// last_checked
// created

    {id: 'campaign_id', label: 'Ид Кампании', minWidth: 100},
    {id: 'campaign_name', label: 'Название кампании'},
    {id: 'status', label: 'Статус', status_label: true},
    {id: 'has_budget', label: 'Настроен бюджет', on_label: 'Настроен', off_label: 'Не настроен'},
    {id: 'has_strategy', label: 'Настроены стратегии', on_label: 'Настроены', off_label: 'Не настроены'},
    {id: 'has_utm', label: 'Все объявления с UTM разметкой', on_label: 'Да', off_label: 'Не все'},
    {id: 'last_checked', label: 'Дата последней проверки'},

];

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: '90vh',
    },
    row: {
        cursor: "pointer"
    }
});

export default function Campaigns() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const {account_id, client_id} = useParams();

    const list_campaigns = useSelector(state => state.campaigns.list)
    const error = useSelector(state => state.campaigns.error)
    const is_loaded = useSelector(state => state.campaigns.is_loaded)


    useEffect(() => {
        dispatch(getCampaigns(client_id))
    }, [])

    if (error !== undefined) {
        return <AlertError error={error}/>
    }


    if (list_campaigns === undefined) {
        return <LinearProgress/>
    }

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const list_campaigns_filtered = list_campaigns
        .filter(e => e.client_id === client_id && e.status !== 'Archived')


    return <>

        <Typography variant="h4" component="h2" gutterBottom>
            Рекламные кампании клиента
        </Typography>

        <Typography variant="body1" gutterBottom> Клиент ИД: {client_id}</Typography>
        <Typography variant="caption" component="p" paragraph={true}>
            Проверьте, что ваши кампании настроены верно:
        </Typography>
        {list_campaigns_filtered.length === 0 && (
            <Box mt={5}>
                <Paper>
                    <Box p={3}>
                        <Typography variant="h5" gutterBottom>Не найдены кампании клиента</Typography>
                        <Typography variant="caption" gutterBottom>
                            Если вы подключили аккаунт недавно, то требуется 2 часа на загрузку информации по кампаниям.<br/>
                            Попробуйте еще раз посетить эту страницу позже.
                        </Typography>


                        <Box mt={3}>
                            <Button title="Перейти к списку аккаунтов"
                                    color="primary"
                                    variant="contained"
                                    component={Link}
                                    to={'/account'}
                            >Перейти к списку аккаунтов</Button>
                        </Box>
                    </Box>
                </Paper>
            </Box>
        )}
        {list_campaigns_filtered.length > 0 && (
            <Paper className={classes.root}>
                {!is_loaded && <CircularProgress/>}
                {is_loaded &&
                <>
                    <TableContainer className={classes.container}>
                        <Table stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                    {columns.map((column) => (
                                        <TableCell
                                            key={column.id}
                                            align={column.align}
                                            style={{minWidth: column.minWidth}}
                                        >
                                            {column.label}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {list_campaigns_filtered
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((acc) => {
                                        return (
                                            <TableRow
                                                hover
                                                role="checkbox"
                                                className={classes.row}
                                                tabIndex={-1}
                                                key={acc.campaign_id}>
                                                {columns.map((column) => {
                                                    const value = acc[column.id];
                                                    return (
                                                        <TableCell key={column.id} align={column.align}>
                                                            <LabelValue label_value={value}
                                                                        on_label={column.on_label}
                                                                        off_label={column.off_label}/>
                                                        </TableCell>
                                                    );
                                                })}
                                            </TableRow>
                                        );
                                    })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={list_campaigns_filtered.length}
                        rowsPerPage={rowsPerPage}
                        labelRowsPerPage='Кампаний на странице'
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </>
                }
            </Paper>
        )}
    </>
}
import Typography from "@mui/material/Typography";
import React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import {Helmet} from "react-helmet";
import SettingsImage from "../../assets/images/settings.png";
import AdCampaignsImage from "../../assets/images/advertising_campaigns.png";
import AdCampaignsStatusImage from "../../assets/images/doc_campaigns.jpg";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles((theme) => ({
    image: {
        width: "100%"
    },
    text_offset: {
        paddingTop: "10px"
    }
}));


export default function Documentation() {
    const classes = useStyles();

    return (
        <Box mt={5}>
            <Helmet>
                <title>Пользовательская документация</title>
            </Helmet>
            <Typography variant="h3" component="h3" align="left">Пользовательская документация АСУР
                Adigital</Typography>

            <Box mt={5}>
                <Typography variant="body1">
                    АСУР Adigital представляет собой ПО для сбора, хранения, обработки,
                    анализа, визуализации данных с рекламных систем.
                </Typography>
                <Typography variant="body1">
                    Основное назначение: отслеживание ключевых параметров рекламных
                    кампаний
                    а также управление статусом (остановкой) этих рекламных кампаний.
                </Typography>
            </Box>

            <Box mt={5}>
                <Typography variant="h4" component="h4" align="left">Авторизация</Typography>
                <Typography className={classes.text_offset} variant="body1">
                    Для начала работы с АСУР Adigital вам необходимо авторизоваться
                    с помощью gmail-аккаунта на странице: &nbsp;
                    <a href="https://asur-adigital.ru/login">https://asur-adigital.ru/login</a>
                </Typography>
            </Box>

            <Box mt={4}>
                <Typography variant="h4" component="h4" align="left">Интеграция рекламных систем</Typography>
                <Typography className={classes.text_offset} variant="body1">
                    Перейдите в раздел Настройки для интеграции рекламных систем и нажмите
                    кнопку “Подключить” у нужной вам рекламной системы.
                </Typography>
                <Paper elevation={2}>
                    <img className={classes.image} src={SettingsImage} alt="Изображение страницы настроек"/>
                </Paper>
            </Box>
            <Box mt={4}>
                <Typography variant="body1">
                    В течение часа в проект подгрузятся ваши рекламные
                    кампании или список клиентов, если вы подключили
                    агентский рекламный аккаунт.
                </Typography>
                <Typography className={classes.text_offset} variant="body1" gutterBottom>
                    Выберите логин клиента, рекламные кампании которого вы хотите проверить.
                </Typography>
                <Paper elevation={2}>
                    <img className={classes.image} src={AdCampaignsImage}
                         alt="Изображение страницы рекламных кампаний"/>
                </Paper>
            </Box>
            <Box mt={4}>
                <Typography variant="body1" >
                    По каждой запущенной рекламной кампании вы получите
                    актуальный статус их проверки и дату последней проверки.
                </Typography>
                 <Paper elevation={2} >
                <img className={classes.image} src={AdCampaignsStatusImage} alt="Изображение страницы настроек"/>
                 </Paper>
            </Box>
            <Box mt={4}>
                <Typography variant="body1">
                    При наличии проблем, отчет об ошибках будет выслан вам на почту автоматически.
                </Typography>
            </Box>
        </Box>
    )
}
import React from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import {Helmet} from "react-helmet";

const Contacts = () => {
    return <>
        <Helmet>
            <title>Реквизиты ООО АДИДЖИТАЛ</title>
        </Helmet>
        <Box display={"flex"} flexDirection={"column"} alignItems={'center'}>
            <Typography variant='h3' mt={5} mb={3}>РЕКВИЗИТЫ</Typography>
            <Typography variant='h1' mb={3}
                        fontWeight={"bold"}
                        sx={{fontSize: {'xs': '3rem', 'md': '6rem'}, textAlign: 'center'}}>ООО АДИДЖИТАЛ</Typography>
            <Typography variant='h6'>ИНН 6685145735</Typography>
            <Typography variant='h6'>ОГРН 1186658013013</Typography>
        </Box>

        <Box display={"flex"} mt={8} justifyContent={"space-between"}>
            <Box>
                <Typography variant='h4' align='center'>Юридический адрес</Typography>
                <Typography mt={2} align='center'>620075, г. Екатеринбург,<br/> ул. Малышева, д. 71, оф. 701</Typography>
            </Box>


            <Box>
                <Typography variant='h4' align='center'>Электронная почта</Typography>
                <Typography mt={2} align='center'><a href="mailto:info@asur-adigital.ru">info@asur-adigital.ru</a></Typography>
            </Box>
        </Box>

    </>
}

export default Contacts
import {applyMiddleware, createStore} from 'redux'
import {rootReducer} from '../redusers/index'
import {createLogger} from 'redux-logger'
import thunk from 'redux-thunk'
import  {GET_FLASH_MESSAGES} from "../constants/flash_messages";


export default function applicationStore() {

    let middleware = [thunk];

    if (process.env.NODE_ENV !== 'production') {
         const logger = createLogger({predicate: (getState, action) => action.type !== GET_FLASH_MESSAGES});
         middleware.push(logger)
    }

    return createStore(
        rootReducer,
        applyMiddleware(...middleware)
    );
}
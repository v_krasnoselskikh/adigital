export const fetchApiResult = (res) => {
    return new Promise((resolve, reject) => {
        if (res.status !== 200) reject(Error(`Код ответ от сервера: ${res.status}`))
        return resolve(res.json())
    })
}
import {fetchApiResult} from "./__errorJsonApi";
import {CHECK_LOGIN, LOGOUT} from '../constants/login';

export function checkLogin() {
    return dispatch => {
        dispatch({type: CHECK_LOGIN})
        return fetch('/google/user_info')
            .then(fetchApiResult)
            .then(body => dispatch({type: CHECK_LOGIN, body}))
            .catch(ex => dispatch({type: CHECK_LOGIN, ex}))
    }
}

export function logout(callback = undefined) {
    return dispatch => {
        dispatch({type: LOGOUT})
        return fetch('/google/logout')
            .then(fetchApiResult)
            .then(body => {
                    dispatch({type: LOGOUT, body})
                    if (typeof callback === 'function') {
                        callback()
                    }
                }
            )
            .catch(ex => dispatch({type: LOGOUT, ex}))
    }
}
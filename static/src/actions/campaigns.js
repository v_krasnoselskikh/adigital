import {GET_CAMPAIGNS} from "../constants/campaigns";
import {fetchApiResult} from "./__errorJsonApi";

export function getCampaigns(client_id) {
    return dispatch => {
        dispatch({type: GET_CAMPAIGNS})
        return fetch(`/clients/${client_id}/campaigns`)
            .then(fetchApiResult)
            .then(body => dispatch({type: GET_CAMPAIGNS, client_id, body}))
            .catch(ex => dispatch({type: GET_CAMPAIGNS, client_id, ex}))
    }
}
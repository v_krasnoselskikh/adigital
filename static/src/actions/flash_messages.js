import {fetchApiResult} from "./__errorJsonApi";
import {GET_FLASH_MESSAGES} from "../constants/flash_messages";


export function getFlashedMessages() {
    return dispatch => {
        dispatch({type: GET_FLASH_MESSAGES})
        return fetch('/get_flashed_messages').
            then(fetchApiResult)
            .then(body => dispatch({type: GET_FLASH_MESSAGES, body}))
            .catch(ex => dispatch({type: GET_FLASH_MESSAGES, ex: ex}))
    }
}
import {GET_PROJECTS} from "../constants/projects";
import {fetchApiResult} from "./__errorJsonApi";

export function getProjects() {
    return dispatch => {
        dispatch({type: GET_PROJECTS})
        return fetch('/projects/')
            .then(fetchApiResult)
            .then(body => dispatch({type: GET_PROJECTS, body}))
            .catch(ex => dispatch({type: GET_PROJECTS, ex}))
    }
}
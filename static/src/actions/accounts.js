import {fetchApiResult} from "./__errorJsonApi";
import {GET_ACCOUNTS, GET_ACCOUNT_CLIENTS, SAVE_ACCOUNT_CLIENTS, DELETE_ACCOUNT} from "../constants/accounts";


export function getAccounts() {
    return dispatch => {
        dispatch({type: GET_ACCOUNTS})
        return fetch('/accounts/').then(fetchApiResult)
            .then(body => dispatch({type: GET_ACCOUNTS, body}))
            .catch(ex => dispatch({type: GET_ACCOUNTS, ex: ex}))
    }
}

export function getAccountClients(account_id) {
    return dispatch => {
        dispatch({type: GET_ACCOUNT_CLIENTS})
        return fetch(`/accounts/${account_id}/clients`)
            .then(fetchApiResult)
            .then(body => dispatch({type: GET_ACCOUNT_CLIENTS, account_id, body}))
            .catch(ex => dispatch({type: GET_ACCOUNT_CLIENTS, account_id, ex}))
    }
}


export function saveAccountClients(account_id) {
    return dispatch => {
        dispatch({type: SAVE_ACCOUNT_CLIENTS})
        return fetch(`/${account_id}/clients`)
            .then(fetchApiResult)
            .then(body => dispatch({type: SAVE_ACCOUNT_CLIENTS, body}))
            .catch(ex => dispatch({type: SAVE_ACCOUNT_CLIENTS, ex}))
    }
}

export function deleteAccount(account_id) {
    return dispatch => {
        dispatch({type: DELETE_ACCOUNT, account_id})
        return fetch(`/accounts/${account_id}`, {method: 'DELETE'})
            .then(fetchApiResult)
            .then(body => dispatch({type: DELETE_ACCOUNT, body, account_id}))
            .catch(ex => dispatch({type: DELETE_ACCOUNT, ex, account_id}))
    }
}
import {createTheme, responsiveFontSizes} from "@mui/material/styles";
import {blue, green, orange} from "@mui/material/colors";

export const theme = responsiveFontSizes(createTheme({
    palette: {
        primary: {
            light: blue[100],
            main: '#4154b3',
            dark: blue[800]
        },
        info: {
            light: green[500],
            main: green[600],
            dark: green[800]
        }
    },
    status: {
        danger: orange[500],
    },
}));

// theme.typography.h1 = {
//   fontSize: '1.2rem',
//   '@media (min-width:600px)': {
//     fontSize: '1.5rem',
//   },
//   [theme.breakpoints.up('md')]: {
//     fontSize: '2.4rem',
//   },
// };
import React from "react";
import {Provider} from "react-redux";
import {ThemeProvider} from "@mui/material/styles";
import {theme} from "./theme";
import applicationStore from "./store";
import AppRoutes from "./components/AppRoutes";

const store = applicationStore();

export default function App() {
    return <Provider store={store}>
        <ThemeProvider theme={theme}>
            <AppRoutes/>
        </ThemeProvider>
    </Provider>
}
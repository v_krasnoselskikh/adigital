import {combineReducers} from 'redux'
import login from './login'
import project from "./projects";
import accounts from "./accounts";
import clients from "./clients";
import campaigns from "./campaigns";
import messages from "./flash_messages";


export const rootReducer = combineReducers({
    login,
    project,
    accounts,
    clients,
    messages,
    campaigns
})

import {GET_FLASH_MESSAGES} from "../constants/flash_messages";
import {union} from 'lodash/array'

const initial_state = {
    list: []
}
const messages = (state = initial_state, action) => {
    if (action.ex) {
        return {...state, error: action.ex}
    }

    switch (action.type) {
        case GET_FLASH_MESSAGES:
            if (action.body) {
                return {...state, list: action.body}
            }
            return state
        default:
            return state
    }
}

export default messages
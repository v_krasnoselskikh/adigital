// account_id
// system
// account_name
// account_type
// created


import {GET_ACCOUNTS, DELETE_ACCOUNT} from '../constants/accounts'

const initial_state = {
    list: undefined,
    error: undefined
}
const accounts = (state = initial_state, action) => {
    if (action.ex) {
        return {...state, error: action.ex}
    }
    switch (action.type) {
        case GET_ACCOUNTS:
            if (action.body) {
                return {...state, list: action.body}
            }
            return state
        case DELETE_ACCOUNT:
            if (action.body){
                return {...state, list: state.list.filter(e=>e.account_id !== action.account_id)}
            }
        default:
            return state
    }
}

export default accounts
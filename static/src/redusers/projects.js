import {GET_PROJECTS} from '../constants/projects'

const initial_state = {
    id: undefined,
    name: undefined,
    register_date: undefined,
    error: false
}
const project = (state = initial_state, action) => {
    switch (action.type) {
        case GET_PROJECTS:
            if (action.body){
                return {...state, ...action.body}
            }
            if (action.ex){
                return {...state, error: action.ex}
            }
            return state
        default:
            return state
    }
}

export default project
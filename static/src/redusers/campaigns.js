import {GET_CAMPAIGNS} from "../constants/campaigns";
import {unionBy} from 'lodash/array'

const initial_state = {
    list: undefined,
    error: undefined,
    is_loaded: undefined
}
const campaigns = (state = initial_state, action) => {


    switch (action.type) {
        case GET_CAMPAIGNS:
            if (action.body) {
                return {
                    ...state,
                    is_loaded: true,
                    list: unionBy(
                        state.list || [],
                        action.body.result.map(e=>({...e, client_id: action.client_id})),
                        'campaign_id'
                    )}
            }

            if (action.ex) {
                return {...state, error: action.ex, is_loaded: true}
            }
            return {...state, error: action.ex, is_loaded: false}
        default:
            return state
    }
}

export default campaigns
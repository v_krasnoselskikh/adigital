import {CHECK_LOGIN, LOGOUT} from '../constants/login'
const initial_state = {
    is_login: undefined,
    user_info: {},
    error: false
}
const login = (state = initial_state, action) => {
    switch (action.type) {
        case CHECK_LOGIN:
            if (action.body){
                return {...state, ...action.body}
            }
            if (action.ex){
                return {...state, error: action.ex}
            }
            return state
        case LOGOUT:
            if (action.body){
                return {...state, user_info:{}, is_login: false}
            }
            if (action.ex){
                return {...state, error: action.ex}
            }
            return state
        default:
            return state
    }
}

export default login

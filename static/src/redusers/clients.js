import {GET_ACCOUNT_CLIENTS} from "../constants/accounts";
import {unionBy} from 'lodash/array'

const initial_state = {
    list: undefined,
    error: undefined,
    is_loaded: undefined
}
const clients = (state = initial_state, action) => {


    switch (action.type) {
        case GET_ACCOUNT_CLIENTS:
            if (action.body) {
                return {...state, is_loaded: true, list: unionBy(state.list || [], action.body, 'client_id')}
            }

            if (action.ex) {
                return {...state, error: action.ex, is_loaded: true}
            }

            return {...state, error: action.ex, is_loaded: false}
        default:
            return state
    }
}

export default clients
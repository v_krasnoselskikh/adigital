import unittest
from app.controllers.email import send_email
from start import app


class EmailSendTest(unittest.TestCase):
    client = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.client =  app.test_client()

    def test_email_send(self):
        send_email(to='kv@good.bi', content='test')

    def test_endpoint_send_email_without_args(self):
        r = self.client.post('/email/request-for-connection', json=dict())
        self.assertEqual(r.status_code, 400)

    def test_endpoint_send_email_with_args(self):
        r = self.client.post('/email/request-for-connection', json=dict(
            phone='9993033033',
            email='kv@mail.ru',
            name='kv'
        ))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json['status'], 'ok')

from app.api.google_ads.method import get_ads, save_ads
from tests.google_ads_tests.basic import BasicGoogleAdsTest


class AdTest(BasicGoogleAdsTest):
    def test_method(self):
        result = get_ads(self.client, customer_id=self.customer)
        assert isinstance(result, list), 'Argument of wrong type!'

    def test_save(self):
        save_ads(self.client)

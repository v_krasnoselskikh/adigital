from app.api.google_ads.method import get_list_accessible_accounts, get_account_information, save_clients
from tests.google_ads_tests.basic import BasicGoogleAdsTest


class TestGetClientsGoogleAds(BasicGoogleAdsTest):

    def test_get_list_accessible_accounts(self):
        acc = get_list_accessible_accounts(self.client)
        self.assertIsInstance(acc, list)
        print(acc)

    def test_get_account_information(self):
        info = get_account_information(self.client, self.customer)
        self.assertIsInstance(info, dict)

        self.assertIn('customer_id', info)
        self.assertIsNotNone(info['customer_id'])
        self.assertIn('descriptive_name', info)
        self.assertIsNotNone(info['descriptive_name'])
        print(info)

    def test_save_clients(self):
        save_clients(self.client, self.test_acc_id)

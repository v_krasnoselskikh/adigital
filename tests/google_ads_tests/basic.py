import unittest

from app.api.google_ads.api import get_client_by_google_account_id


class BasicGoogleAdsTest(unittest.TestCase):
    test_acc_id = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.test_acc_id = '109550690248207587525'
        cls.customer = '3609144515'
        cls.client = get_client_by_google_account_id(cls.test_acc_id)
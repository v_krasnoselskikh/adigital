from tests.google_ads_tests.basic import BasicGoogleAdsTest
from app.api.google_ads.method import get_campaigns, save_campaigns, get_campaigns_budget, save_google_campaigns_budget


class CampaignsTest(BasicGoogleAdsTest):
    def test_method(self):
        result = get_campaigns(self.client, customer_id=self.customer)
        assert isinstance(result, list), 'Argument of wrong type!'

    def test_get_campaigns_budget(self):
        info = get_campaigns_budget(self.client, self.customer)
        print(info)

    def test_save(self):
        save_campaigns(self.client)

    def test_save_campaigns_budget(self):
        save_google_campaigns_budget(self.client)


from app.api.google_ads.method import get_ad_groups, save_ad_groups
from tests.google_ads_tests.basic import BasicGoogleAdsTest


class AdgroupsTest(BasicGoogleAdsTest):
    def test_method(self):
        result = get_ad_groups(self.client, customer_id=self.customer)
        assert isinstance(result, list), 'Argument of wrong type!'

    def test_save(self):
        save_ad_groups(self.client)

import unittest
from app.types import NameSystemType, AccountType
from datetime import datetime
from app.controllers.accounts import Account


class CreateAccountTest(unittest.TestCase):
    """ Тест работы контроллеров Аккаунтов и Доступов"""

    def setUp(self) -> None:
        self.test_account = dict(
            system=NameSystemType.YandexDirect,
            account_id='test',
            account_name='test_name',
            account_type=AccountType.Agency,
        )
        self.test_credentials = dict(
            access_token='test access',
            refresh_token='test refresh',
            token_uri='test_uri',
            scopes='test_scopes',
            expires_in=datetime(2020, 1, 1),
        )

    def test_create_account(self):
        acc = Account.create_or_update(**self.test_account)
        acc.set_credential(**self.test_credentials)

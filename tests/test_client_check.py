from unittest import TestCase
from app.controllers.clients import Client


class TestClientCheck(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.test_client = '3609144515'

    def test_check_client(self):
        cli = Client(self.test_client)
        cli.check()

import unittest
from os import getenv
from app.controllers import Account

from app.api.direct.method import get_clients, save_clients


class ClientsDirect(unittest.TestCase):
    def setUp(self) -> None:
        self.login = getenv('TEST_DIRECT_ACCOUNT_LOGIN')
        self.account = Account(self.login)

    def test_get_clients(self):
        clients = get_clients(self.login)
        self.assertIsInstance(clients, list, 'Долженн быть получен список клиентов')
        for client in clients:
            self.assertIn('Login', client)
            self.assertIn('ClientInfo', client)
            self.assertIn('Archived', client)

    def test_save_clients(self):
        save_clients(self.login)
import unittest
from os import getenv

from app.api.direct.method import get_ad_groups, save_ad_groups


class AdGroupsDirect(unittest.TestCase):
    def setUp(self) -> None:
        self.client = getenv('TEST_DIRECT_CLIENT_LOGIN')
        self.campaign_id = getenv('TEST_DIRECT_CAMPAIGN')

    def test_get_ad_groups(self):
        ad_groups = get_ad_groups([self.campaign_id])
        self.assertIsInstance(ad_groups, list, 'Долженн быть получен список групп объявлений')
        for ad_group in ad_groups:
            self.assertIn('Id', ad_group)
            self.assertIn('Name', ad_group)
            self.assertIn('CampaignId', ad_group)

    def test_save_ad_groups(self):
        save_ad_groups(self.client, [self.campaign_id])
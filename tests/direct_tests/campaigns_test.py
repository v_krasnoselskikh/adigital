import unittest
from os import getenv

from app.types import NameSystemType
from app.controllers import Client
from app.api.direct.method import get_campaigns, save_campaigns


class CampaignsDirect(unittest.TestCase):
    def setUp(self) -> None:
        self.client = getenv('TEST_DIRECT_CLIENT_LOGIN')
        self.account = Client(self.client)

    def test_get_campaigns(self):
        campaigns = get_campaigns(self.client)
        self.assertIsInstance(campaigns, list, 'Долженн быть получен список кампаний')
        for campaign in campaigns:
            self.assertIn('Id', campaign)
            self.assertIn('Name', campaign)
            self.assertIn('State', campaign)

    def test_save_campaigns(self):
        save_campaigns(self.client)
import unittest
from os import getenv

from app.api.direct.method import get_ads, save_ads


class AdsDirect(unittest.TestCase):
    def setUp(self) -> None:
        self.client = getenv('TEST_DIRECT_CLIENT_LOGIN')
        self.ad_group_id = getenv('TEST_DIRECT_AD_GROUP')

    def test_get_ads(self):
        ads = get_ads([self.ad_group_id])
        self.assertIsInstance(ads, list, 'Долженн быть получен список объявлений')
        for ad in ads:
            self.assertIn('Id', ad)
            self.assertIn('Status', ad)
            self.assertIn('AdGroupId', ad)

    def test_save_ads(self):
        save_ads(self.client, [self.ad_group_id])
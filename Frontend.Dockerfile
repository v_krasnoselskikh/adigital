FROM node:alpine
WORKDIR /workspace

COPY /static/package.json /workspace/package.json
CMD npm install --legacy-peer-deps && npx webpack -d --watch
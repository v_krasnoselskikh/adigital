# setuptools.setup(
#     name="adigital",
#     version="0.0.1",
#     author="Artsofte",
#     author_email="",
#     description="Routes for check Ads campaigns",
#     url="https://bitbucket.org/calleadka/adigital/src/master/",
#     packages=setuptools.find_packages(),
#     classifiers=[
#         "Programming Language :: Python :: 3",
#         "License :: CC BY-NC-SA",
#         "Operating System :: OS Independent",
#     ],
#     python_requires='>=3.7',
# )

from logging_config import ROOT_LOGGER

LOGGER = ROOT_LOGGER.getChild('Setup')


def setup_database():
    """ Первичная настройка БД. Можно вызввать функцию повторно"""
    from app.models import create_tables
    from app.types.systems import NameSystemType

    LOGGER.info('Создаем / проверяем таблицы')
    create_tables()
    LOGGER.info('Создаем типы систем')
    NameSystemType.create_in_db()

    # Помещаем значения в БД по умолчанию


setup_database()
